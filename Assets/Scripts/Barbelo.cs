﻿using Mirror;
using SpiritWorld.Events;
using SpiritWorld.Game.Data;
using SpiritWorld.Managers;
using SpiritWorld.World.Entities.Creatures;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace SpiritWorld {

  /// <summary>
  /// The server Universe object. Exposes Managers which have commands that Saturn can use to make requests.
  /// </summary>
  public static class Barbelo {

    /// <summary>
    /// The system to send notification between controllers and managers across different channels.
    /// Used to receive notifications of changes to be made from the server
    /// </summary>
    public static WorldScapeEventSystem EventSystem;

    /// <summary>
    /// The player Managers
    /// </summary>
    public static Dictionary<Player, PlayerManager> PlayerManagers {
      get;
    } = new Dictionary<Player, PlayerManager>();

    /// <summary>
    /// The manager of the active scape
    /// </summary>
    public static ScapeManager ActiveScapeManager {
      get;
      private set;
    }

    /// <summary>
    /// Local player's manager.
    /// </summary>
    public static PlayerManager LocalPlayerManager
      => PlayerManagers.Values.Single(pm => pm.player == Saturn.LocalPlayerController.player);

    /// <summary>
    /// get a current UUID
    /// </summary>
    /// <returns></returns>
    public static string UUID 
      => GenNextUUID();

    /// <summary>
    /// Set the active scape
    /// </summary>
    /// <param name="scapeManager"></param>
    [Command]
    public static void SetActiveScape(ScapeManager scapeManager, ScapeProfile scapeProfile) {
      ActiveScapeManager = scapeManager;
      scapeManager.initialize(scapeProfile);
    }

    /// <summary>
    /// Generate the UUID, server only
    /// </summary>
    [Server]
    static string GenNextUUID() {
      return $"E{new DateTimeOffset(DateTime.Now).ToUnixTimeMilliseconds().ToString()}" +
      $"V{UnityEngine.Random.Range(0, int.MaxValue)}" +
      $"I{Saturn.LoadedProfiles.CurrentUser?.name ?? "THEVOID"}" +
      $"X{SystemInfo.deviceUniqueIdentifier}";
    }
  }
}
