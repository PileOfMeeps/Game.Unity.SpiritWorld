﻿using SpiritWorld.Inventories;
using SpiritWorld.Inventories.Items;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SpiritWorld.Game.Controllers {

  /// <summary>
  /// A controller for a shaped grid pack of items
  /// </summary>
  [RequireComponent(typeof(GridLayoutGroup))]
  public class GridPackController : MonoBehaviour {

    #region Constants

    /// <summary>
    /// Grid item slot prefab
    /// </summary>
    public ItemSlotController GridItemSlotPrefab;

    #endregion

    /// <summary>
    /// The grid pack slots by coordinate location
    /// </summary>
    ItemSlotController[,] gridPackSlots;

    /// <summary>
    /// Items based on pivot point
    /// </summary>
    Dictionary<Coordinate, ItemSlotController> itemsByPivot
      = new Dictionary<Coordinate, ItemSlotController>();

    /// <summary>
    /// The inventoy this is for.
    /// </summary>
    ShapedPack inventory;

    /// <summary>
    /// This's transform
    /// </summary>
    GridLayoutGroup gridGroup
      => _gridGroup ?? (_gridGroup = GetComponent<GridLayoutGroup>());
    GridLayoutGroup _gridGroup;

    /// <summary>
    /// Inventory type if there is one
    /// </summary>
    Enum inventoryType;

    #region Initialization

    /// <summary>
    /// Set up this gridpack
    /// </summary>
    public virtual void initialize(ShapedPack managedInventory, Enum inventoryType = null) {
      inventory = managedInventory;
      this.inventoryType = inventoryType;
      populateGridSlots();
    }

    /// <summary>
    /// Populate the grid with it's slots.
    /// </summary>
    void populateGridSlots() {
      gridGroup.constraintCount = inventory.dimensions.x;
      gridPackSlots = new ItemSlotController[inventory.dimensions.x, inventory.dimensions.y];
      Coordinate.Zero.until(inventory.dimensions, gridLocation => {
        // see if we have an item at this pivot point.
        Item itemAtSlot = inventory.getItemStackAt(gridLocation, out Coordinate itemPivot);
        bool isItemsPivot = gridLocation.Equals(itemPivot);
        if (!isItemsPivot) {
          itemAtSlot = null;
        }
        // add the grid item
        gridPackSlots[gridLocation.x, gridLocation.y] = ItemSlotController.Make(
          transform,
          GridItemSlotPrefab,
          gridLocation,
          inventoryType,
          itemAtSlot
        );
        if (isItemsPivot) {
          itemsByPivot.Add(itemPivot, gridPackSlots[gridLocation.x, gridLocation.y]);
        }
      });
    }

    #endregion

    /// <summary>
    /// Update the slot icon at the given location
    /// </summary>
    /// <param name="slotLocation"></param>
    public void updateSlot(Coordinate slotLocation) {
      gridPackSlots[slotLocation.x, slotLocation.y].updateItemIcon(inventory.getItemStackAt(slotLocation));
    }
  }
}
