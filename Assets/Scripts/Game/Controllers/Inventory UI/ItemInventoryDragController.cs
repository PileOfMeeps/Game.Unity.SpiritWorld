﻿using SpiritWorld.Inventories.Items;
using SpiritWorld.World.Entities.Creatures;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace SpiritWorld.Game.Controllers {

  /// <summary>
  /// controls an item being able to be dragged and dropped into inventories
  /// </summary>
  public class ItemInventoryDragController : EventTrigger {

    #region Constants

    /// <summary>
    /// The item currently beign dragged. only one at a time
    /// </summary>
    public static ItemInventoryDragController ItemBeingDragged
      = null;

    /// <summary>
    /// If an item is being dragged by one of these controllers
    /// </summary>
    public static bool AnItemIsBeingDragged
      => ItemBeingDragged != null;

    #endregion

    /// <summary>
    /// The gameobject this is picked up from, will be destroyed.
    /// </summary>
    public ItemDropController itemPickupSource;

    /// <summary>
    /// The stack id this controller represents
    /// </summary>
    public Player.InventoryTypes parentInventory {
      get;
      private set;
    }

    /// <summary>
    /// the grid location of the item before moving
    /// </summary>
    public Coordinate originalGridLocation {
      get;
      private set;
    }

    /// <summary>
    /// If this is the item being dragged at the moment
    /// </summary>
    public bool isBeingDragged 
      = false;

    /// <summary>
    /// The item type that's being dragged.
    /// </summary>
    public Item.Type type
      => parentController.item?.type;

    /// <summary>
    /// The inital location before being dragged
    /// </summary>
    Vector3 originalTransformPosition;

    /// <summary>
    /// The original scale this icon was at.
    /// </summary>
    float originalScale;

    /// <summary>
    /// The opacity before we started dragging the icon
    /// </summary>
    float originalOpacity;

    /// <summary>
    /// If this icon was originally shaped
    /// </summary>
    bool originallyWasShaped;

    /// <summary>
    /// The parent item icon controller
    /// </summary>
    ItemIconController parentController;

    /// <summary>
    /// The parent before we started dragging
    /// </summary>
    Transform originalParent;

    /// <summary>
    /// The original inventory this was parented to
    /// </summary>
    Player.InventoryTypes originalContainerInventory;

    /// <summary>
    /// initialize this for the given item
    /// </summary>
    /// <param name="parent"></param>
    /// <param name="stackId"></param>
    public void initialize(ItemIconController parent, Coordinate gridLocation, Enum containingInventory, ItemDropController itemPickupSource = null) {
      parentController = parent;
      originalGridLocation = gridLocation;
      this.itemPickupSource = itemPickupSource;
      parentInventory = (Player.InventoryTypes)(containingInventory ?? Player.InventoryTypes.None);
    }

    /// <summary>
    /// Drag the item around
    /// </summary>
    public override void OnDrag(PointerEventData eventData) {
      if (isBeingDragged) {
        Canvas uiCanvas 
          = Saturn.LocalPlayerController.InventoryUIController.getControllerFor(
            parentInventory == Player.InventoryTypes.None 
              ? Player.InventoryTypes.GridPack 
              : parentInventory
            ).canvas;

        // grab it
        RectTransformUtility.ScreenPointToLocalPointInRectangle(
          uiCanvas.transform as RectTransform,
          Input.mousePosition,
          uiCanvas.worldCamera,
          out Vector2 pos
        );
        transform.position = uiCanvas.transform.TransformPoint(pos);

        /// when the pack is open, change the icon type around from shaped to nonshaped.
        if (Saturn.LocalPlayerController.InventoryUIController.packMenuIsOpen) {
          float screenCenter = (Screen.width * 0.5f);
          // on the right side of the screen
          if (Input.mousePosition.x > screenCenter && parentController.isShaped) {
            parentController.setShaped(false);
          } else if (Input.mousePosition.x < screenCenter && !parentController.isShaped) {
            parentController.setShaped(true);
          }
        } else if (parentController.isShaped) {
          parentController.setShaped(false);
        }

        // if we're in item bar teritory, stimulate hovering.
        if (!Saturn.LocalPlayerController.InventoryUIController.packMenuIsOpen || !parentController.isShaped) {
          Saturn.LocalPlayerController.InventoryUIController.HotBarController.updateForItemHoverOver(eventData.position);
        }
      }
    }

    /// <summary>
    /// set being dragged
    /// </summary>
    /// <param name="eventData"></param>
    public override void OnPointerDown(PointerEventData eventData) {
      if (!AnItemIsBeingDragged) {
        enableDrag();
      }
    }

    /// <summary>
    /// set being let go
    /// </summary>
    /// <param name="eventData"></param>
    public override void OnPointerUp(PointerEventData eventData) {
      if (isBeingDragged) {
        disableDrag();

        /// check if we're dropping it into the shaped inventory.
        if (parentController.isShaped && !(parentController.item.type is IShortcut)) {
          // first check to see if we dropped it in the pockets
          if (Saturn.LocalPlayerController.InventoryUIController.PocketsController.tryToGetClosestPocketSlot(eventData.position, out Coordinate nearestSlot)) {
            tryToAddToInventoryGridSlot(Player.InventoryTypes.Pockets, nearestSlot);
            return;
          }

          //Raycast to try to see what shaped inventory slot we hit.
          List<RaycastResult> results = new List<RaycastResult>();
          EventSystem.current.RaycastAll(eventData, results);
          foreach(RaycastResult result in results) {
            // if we hit an item slot
            if (result.gameObject.GetComponent<ItemSlotController>() is ItemSlotController itemSlot) {
              tryToAddToInventoryGridSlot(
                itemSlot.inventoryType is Player.InventoryTypes inventoryType 
                  ? inventoryType 
                  : Player.InventoryTypes.None,
                itemSlot.gridLocation
              );
              return;
            }
          }
        /// check if we're dropping it into the item bar
        } else if (Saturn.LocalPlayerController.InventoryUIController.HotBarController.tryToGetItemBarHoverPosition(eventData.position, out short barSlotIndex, out _)) {
          Coordinate gridItemLocation = new Coordinate(barSlotIndex, 0);
          tryToAddToInventoryGridSlot(Player.InventoryTypes.HotBar, gridItemLocation);
          return;
        /// if we missed everything, just drop the item and remove it.
        } else if (parentInventory != Player.InventoryTypes.Shortcuts) {
          // if it already came from a dropped item, we can just destroy the preview icon.
          if (originalContainerInventory == Player.InventoryTypes.None && itemPickupSource != null) {
            itemPickupSource.removePreviewIcon();
          } else {
            Barbelo.LocalPlayerManager.InventoryManager
              .removeFromInventoryAt(originalContainerInventory, originalGridLocation);
          }

          return;
        }

        resetToOriginalPosition();
      }
    }

    /// <summary>
    /// Callback for destroying the item icon and it's pickup source
    /// </summary>
    public void destroyPickupSource() {
      if (itemPickupSource != null) {
        Destroy(itemPickupSource.gameObject);
      }
    }

    /// <summary>
    /// Callback for destroying the item icon and it's pickup source
    /// </summary>
    public void destroyIcon() {
      Destroy(gameObject);
    }

    /// <summary>
    /// Reset the location to when we started dragging
    /// </summary>
    public void resetToOriginalPosition() {
      if (gameObject != null) {
        transform.position = originalTransformPosition;

        parentInventory = originalContainerInventory;
        parentController.parentTo(originalParent, parentInventory != Player.InventoryTypes.Pockets);
        parentController.setScale(originalScale);
        parentController.setOpacity(originalOpacity);
        if (originallyWasShaped != parentController.isShaped) {
          parentController.setShaped(originallyWasShaped);
        }

        // if we expanded a slot and we're resetting, reset the slot
        Saturn.LocalPlayerController.InventoryUIController.HotBarController.resetPreviewSlot();
      }
    }

    /// <summary>
    /// add this item to the given inventory at the given slot
    /// </summary>
    /// <param name="gridItemLocation"></param>
    void tryToAddToInventoryGridSlot(Player.InventoryTypes inventoryType, Coordinate gridItemLocation) {
      // make sure it's not the same spot.
      if (originalContainerInventory != Player.InventoryTypes.Shortcuts && originalContainerInventory != Player.InventoryTypes.None) {
        Barbelo.LocalPlayerManager.InventoryManager.tryToSwapInventoryItems(
          (originalContainerInventory, originalGridLocation),
          (inventoryType, gridItemLocation)
        );
      } else {
        Barbelo.LocalPlayerManager.InventoryManager.tryToAddToInventoryAt(
          parentController.item,
          (inventoryType, gridItemLocation),
          (originalContainerInventory, originalGridLocation)
        );
      }
    }

    /// <summary>
    /// Turn dragging on
    /// </summary>
    void enableDrag() {
      originalParent = transform.parent;
      originalTransformPosition = transform.position;
      originalScale = parentController.currentScale;
      originalContainerInventory = parentInventory;
      originalOpacity = parentController.currentOpacity;
      originallyWasShaped = parentController.isShaped;

      ItemBeingDragged = this;
      isBeingDragged = true;

      /// update appearence for dragging
      parentController.setOpacity(1);
      parentController.setScale(1);

      // if the pack menu is open it can manage all of the dragging
      if (Saturn.LocalPlayerController.InventoryUIController.packMenuIsOpen 
        && parentInventory != Player.InventoryTypes.GridPack 
        && !(parentController.item.type is IShortcut)
      ) {
        // re-parent to the open grid
        parentInventory = Player.InventoryTypes.GridPack;
        parentController.parentTo(
          Saturn.LocalPlayerController.InventoryUIController.GridPackController.transform.GetChild(0),
          originalContainerInventory != Player.InventoryTypes.Pockets
        );
      }
    }

    /// <summary>
    /// disable the dragging
    /// </summary>
    void disableDrag() {
      ItemBeingDragged = null;
      isBeingDragged = false;
    }
  }
}
