﻿using SpiritWorld.Inventories.Items;
using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace SpiritWorld.Game.Controllers {
  public class ItemSlotController : EventTrigger {

    /// <summary>
    /// Modes for the item slots in the ui
    /// </summary>
    public enum Mode {
      Default, // default mode
      Inspect, // when it's just moused over
      Selected, // item is the selected item
      Hovered // item is being hovered over by another item.
    };

    #region Constants

    /// <summary>
    /// The type of icon that's displayed in this slot by default
    /// </summary>
    public ItemIconController.IconType ItemIconShapeType
      = ItemIconController.IconType.Square;

    /// <summary>
    /// The default color of this slot's bg.
    /// </summary>
    [SerializeField]
    public Color DefaultColor
      = new Color(0, 131, 190);

    /// <summary>
    /// The color of this slot's bg when it's marked as the 'selected' item.
    /// </summary>
    [SerializeField]
    public Color SelectedColor
      = new Color(0, 131, 255);

    /// <summary>
    /// The color of this slot's bg when another item is hovering over it.
    /// </summary>
    [SerializeField]
    public Color HoverColor
      = new Color(80, 131, 30);

    /// <summary>
    /// If we expand the icon on mouseover
    /// </summary>
    [SerializeField]
    public bool ScaleWhenHilighted
      = true;

    /// <summary>
    /// The default scale of the icon
    /// </summary>
    [SerializeField]
    public float DefaultScale
      = 1;

    /// <summary>
    /// The hilighted scale of the icon
    /// </summary>
    [SerializeField]
    public float HilightedScale
      = 1.5f;

    #endregion

    /// <summary>
    /// The grid location of this slot controller (for lines inventories like bars)
    /// </summary>
    public Coordinate gridLocation {
      get;
      private set;
    }

    /// <summary>
    /// The inventory this is for
    /// </summary>
    public Enum inventoryType {
      get;
      private set;
    }

    /// <summary>
    /// The slot index of this slot controller (for lines inventories like bars)
    /// </summary>
    public int slotIndex => gridLocation.x;

    /// <summary>
    /// The current mode
    /// </summary>
    public Mode currentMode {
      get;
      private set;
    } = Mode.Default;

    /// <summary>
    /// The item icon for this slot.
    /// </summary>
    ItemIconController icon;

    /// <summary>
    /// The previous mode, for temp changes
    /// </summary>
    Mode previousMode
      = Mode.Default;

    /// <summary>
    /// The item this slot was spawned from if this came from an item drop.
    /// </summary>
    ItemDropController itemDropSource;

    /// <summary>
    /// Setup
    /// </summary>
    /// <param name="slotLocation"></param>
    void setIndex(Coordinate slotLocation, Enum inventoryType = null) {
      gridLocation = slotLocation;
      this.inventoryType = inventoryType;
    }

    /// <summary>
    /// Make a new item slot.
    /// </summary>
    public static ItemSlotController Make(
      Transform parent,
      ItemSlotController slotPrefab, 
      Coordinate slotLocation,
      Enum inventoryType,
      Item item = null,
      ItemDropController itemDropSource = null
    ) {
      ItemSlotController slot = Instantiate(slotPrefab, parent) as ItemSlotController;
      slot.itemDropSource = itemDropSource;
      slot.gameObject.AddComponent<GraphicRaycaster>();
      slot.setIndex(slotLocation, inventoryType);
      slot.updateItemIcon(item);
      slot.updateMode(Mode.Default);

      return slot;
    }

    /// <summary>
    /// Update the item icon displayed
    /// </summary>
    /// <param name="newItem"></param>
    public void updateItemIcon(Item newItem) {
      bool iconExists = icon != null;
      // if it's all null, we can't change anything.
      if (iconExists && icon.item == null && newItem == null) {
        return;
      // if we're just changing the stack #
      } else if (iconExists && icon.item != null && icon.item.canStackWith(newItem)) {
        icon.updateStackCount();
      } else {
        if (iconExists) {
          Destroy(icon.gameObject);
        }
        icon = newItem == null ? ItemIconController.Make(null, transform, true) : ItemIconController.Make(
          newItem,
          transform,
          true,
          true,
          gridLocation,
          inventoryType,
          itemDropSource
        );
        if (ItemIconShapeType == ItemIconController.IconType.Shaped) {
          icon.setShaped(true);
        }
      }
    }

    /// <summary>
    /// Update the mode of this icon slot
    /// </summary>
    /// <param name="newMode"></param>
    public void updateMode(Mode newMode, bool updateSize = true) {
      if (updateSize && ScaleWhenHilighted) {
        icon.setScale(newMode == Mode.Default || newMode == Mode.Inspect ? DefaultScale : HilightedScale);
      }
      icon.setBGColor(newMode == Mode.Default
        ? DefaultColor
        : newMode == Mode.Hovered || newMode == Mode.Inspect
          ? HoverColor
          : SelectedColor
      );

      currentMode = newMode;
    }

    /// <summary>
    /// Update the alpha of the icon
    /// </summary>
    /// <param name="alpha"></param>
    public void updateOpacity(float alpha) {
      icon.setOpacity(alpha);
    }

    /// <summary>
    /// set mode to inspect, but don't change the size
    /// </summary>
    /// <param name="eventData"></param>
    public override void OnPointerEnter(PointerEventData eventData) {
      if (!icon.isShaped && !ItemInventoryDragController.AnItemIsBeingDragged) {
        previousMode = currentMode;
        updateMode(Mode.Inspect, false);
      }
    }

    /// <summary>
    /// Reset the mode to what it was
    /// </summary>
    /// <param name="eventData"></param>
    public override void OnPointerExit(PointerEventData eventData) {
      if (!icon.isShaped && !ItemInventoryDragController.AnItemIsBeingDragged) {
        updateMode(previousMode, false);
      }
    }

    [CustomEditor(typeof(ItemSlotController))]
    public class ItemSlotControllerEditor : Editor {
      //This method is called every time Unity needs to show or update the 
      //inspector when you select a gameobject with an XXX component.
      public override void OnInspectorGUI() {
        //And then we will draw the inspector for event types and events, which is the default behaviour.
        base.OnInspectorGUI();
      }
    }
  }
}