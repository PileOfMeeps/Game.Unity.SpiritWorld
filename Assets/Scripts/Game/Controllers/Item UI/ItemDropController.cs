﻿using SpiritWorld.Inventories.Items;
using UnityEngine;
using SpiritWorld.Managers;
using SpiritWorld.World.Entities.Creatures;
using SpiritWorld.Events;

namespace SpiritWorld.Game.Controllers {

  /// <summary>
  /// Controller for item drops
  /// </summary>
  public class ItemDropController : MonoBehaviour {

    #region Constants

    /// <summary>
    /// The prefab used to hilight item drops
    /// </summary>
    public GameObject ItemHilight;

    /// <summary>
    /// How fast the hilight rotates around the item
    /// </summary>
    public int HilightRotationSpeed
      = 20;

    /// <summary>
    /// The slot controller for a free floating item icon
    /// </summary>
    public ItemSlotController FreeSlotController;

    /// <summary>
    /// The furthest away a pickup icon can be shown for this
    /// </summary>
    public float MaxPickupIconDistance
      = 20;

    /// <summary>
    /// How long is a double click
    /// </summary>
    const float DoubleClickTime
      = 0.25f;

    #endregion

    /// <summary>
    /// The data for retreiving the item this drop is controlling.
    /// </summary>
    public (int dropId, Coordinate chunkAxialKey) chunkDropData {
      get;
      private set;
    }

    /// <summary>
    /// The slot used to preview the item for pickup
    /// </summary>
    ItemSlotController previewItemSlot;

    /// <summary>
    /// The item used to hilight this item
    /// </summary>
    GameObject itemHilight;

    /// <summary>
    /// The item model for the item
    /// </summary>
    GameObject itemModel;

    /// <summary>
    /// The time we held the mouse down for
    /// </summary>
    float timePassedSinceLastClick = -1;

    #region Initialization

    /// <summary>
    /// Set the current item to display
    /// </summary>
    /// <param name="item"></param>
    public void initialize(Item item, (int dropId, Coordinate chunkAxialKey) chunkDropData) {
      this.chunkDropData = chunkDropData;
      if (item != null && item.type != null) {
        itemModel = Instantiate(ItemDataMapper.GetModelFor(item), transform);
        Collider[] itemColliders = itemModel.GetComponentsInChildren<Collider>(true);
        foreach (Collider itemCollider in itemColliders) {
          itemCollider.enabled = true;
        }
        itemHilight = Instantiate(ItemHilight, transform);
      }
    }

    #endregion

    #region Visual Updates

    /// <summary>
    /// Item hilight effects
    /// </summary>
    void Update() {
      // if we have a preview slot, and get too far away, remove it.
      if (previewItemSlot != null
        && Vector3.Distance(transform.position, Saturn.LocalPlayerController.transform.position) > MaxPickupIconDistance
        // if it's also not the item currently being dragged.
        && !(ItemInventoryDragController.ItemBeingDragged?.parentInventory == Player.InventoryTypes.None
          && previewItemSlot.gridLocation == ItemInventoryDragController.ItemBeingDragged?.originalGridLocation)
      ) {
        removePreviewIcon();
      }

      /// update last click time if we're tracking it
      if (timePassedSinceLastClick >= 0) {
        // if we clicked once and then went passed the double click time, do single click action and show the preview icon
        if (timePassedSinceLastClick >= DoubleClickTime) {
          enablePreviewIcon();
          timePassedSinceLastClick = -1;
        } else {
          timePassedSinceLastClick += Time.deltaTime;
        }
      }

      /// spin the highlight
      if (itemHilight != null) {
        itemHilight.transform.Rotate(HilightRotationSpeed * Time.deltaTime, 0, 0);
      }
    }

    /// <summary>
    /// Create the preview icon and place it where we clicked.
    /// </summary>
    void enablePreviewIcon() {
      previewItemSlot = ItemSlotController.Make(
        Saturn.LocalPlayerController.InventoryUIController.GridPackController.ItemPackMenuController.transform,
        FreeSlotController,
        Coordinate.Zero,
        Player.InventoryTypes.None,
        getDroppedItem(),
        this
      );
      previewItemSlot.transform.position 
        = Saturn.LocalPlayerController.InventoryUIController.GridPackController
          .canvas.worldCamera.ScreenToWorldPoint(Input.mousePosition);
    }

    /// <summary>
    /// Destroy the preview icon.
    /// </summary>
    public void removePreviewIcon() {
      if (previewItemSlot != null) {
        Destroy(previewItemSlot.gameObject);
        previewItemSlot = null;
      }
    }

    #endregion

    #region Interactions

    /// <summary>
    /// Try to add the item to the player's inventory when they walk into it
    /// </summary>
    /// <param name="collision"></param>
    void OnCollisionEnter(Collision collision) {
      if (collision.gameObject.CompareTag("Player")) {
        tryToGetPickedUpBy(collision.gameObject.GetComponent<PlayerManager>());
      }
    }

    /// <summary>
    /// If the local player, start timing the time since last click, if they click again within 
    /// the time it counts as a double click
    /// On double click just try to pick up the item quick
    /// </summary>
    void OnMouseDown() {
      if (previewItemSlot == null) {
        if (timePassedSinceLastClick < DoubleClickTime && timePassedSinceLastClick > 0) {
          tryToGetPickedUpBy(Barbelo.LocalPlayerManager);
          timePassedSinceLastClick = -1;
        }

        timePassedSinceLastClick = 0;
      }
    }

    /// <summary>
    /// When the user lifts the mouse, if we're passed the double click time, hide the preview icon.
    /// </summary>
    void OnMouseUp() {
      if (timePassedSinceLastClick >= DoubleClickTime) {
        removePreviewIcon();
        timePassedSinceLastClick = -1;
      }
    }

    /// <summary>
    /// Try to get picked up by the given player
    /// </summary>
    /// <param name="player"></param>
    void tryToGetPickedUpBy(PlayerManager player) {
      if (player != null) {
        player.InventoryManager.tryToPickUpDrop(chunkDropData);
      }
    }

    #endregion

    #region Data Access

    /// <summary>
    /// Get the dropped item this is controlling
    /// </summary>
    /// <returns></returns>
    Item getDroppedItem() {
      return Barbelo.ActiveScapeManager.activeBoard[chunkDropData.chunkAxialKey].getDrop(chunkDropData.dropId);
    }

    #endregion

    #region Events

    /// <summary>
    /// event for player picking up an item
    /// </summary>
    public struct PickedUpEvent : IEvent {
      public string name
        => "Drop was picked up";

      /// <summary>
      /// The data for retreiving the item this drop is controlling.
      /// </summary>
      public (int dropId, Coordinate chunkAxialKey) chunkDropData {
        get;
        private set;
      }
      /// <summary>
      /// Make an event of this kind
      /// </summary>
      /// <param name="player"></param>
      /// <param name="item"></param>
      public PickedUpEvent((int dropId, Coordinate chunkAxialKey) chunkDropData) {
        this.chunkDropData = chunkDropData;
      }
    }

    #endregion
  }
}