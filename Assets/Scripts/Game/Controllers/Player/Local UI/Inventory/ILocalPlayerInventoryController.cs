﻿using SpiritWorld.Events;
using SpiritWorld.Inventories;
using UnityEngine;

namespace SpiritWorld.Game.Controllers {
  public interface ILocalPlayerInventoryController: IObserver {

    /// <summary>
    /// The canvas for the UI of this inventory
    /// </summary>
    Canvas canvas {
      get;
    }

    /// <summary>
    /// initalize the inventory
    /// </summary>
    /// <param name="managedInventory"></param>
    void initialize(IInventory managedInventory);

    /// <summary>
    /// update the item in the given slot
    /// </summary>
    /// <param name="slotLocation"></param>
    void updateSlot(Coordinate slotLocation);
  }
}