﻿using SpiritWorld.Events;
using SpiritWorld.Inventories;
using SpiritWorld.Inventories.Items;
using SpiritWorld.Managers;
using SpiritWorld.World.Entities.Creatures;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace SpiritWorld.Game.Controllers {

  /// <summary>
  /// The controller for the local player's grid pack menu
  /// </summary>
  public class LocalPlayerGridPackController : GridPackController, ILocalPlayerInventoryController {

    #region Constants

    /// <summary>
    /// The prefab for the shortcut slots
    /// </summary>
    public ItemSlotController ShortcutSlotPrefab;

    /// <summary>
    /// Controls opening and closing of this menu
    /// SET IN UNITY
    /// </summary>
    public ItemPackMenuController ItemPackMenuController;

    /// <summary>
    /// The bar we insert the shortcut icons into.
    /// SET IN UNITY
    /// </summary>
    public Transform ShortcutBar;

    #endregion

    /// <summary>
    /// The canvas for this.
    /// </summary>
    public Canvas canvas
      => _canvas ?? (_canvas = GetComponentInParent<Canvas>());
    Canvas _canvas;

    /// <summary>
    /// The item shortcuts
    /// </summary>
    ItemSlotController[] shortcuts;

    #region Initialization

    /// <summary>
    /// If this has been initalized yet.
    /// </summary>
    bool isInitalized = false;

    /// <summary>
    /// Set up the grid based on player inventory
    /// </summary>
    void Start() {
      if (!isInitalized) {
        initialize(Barbelo.LocalPlayerManager.player.packInventory, Player.InventoryTypes.GridPack);
      }
    }

    /// <summary>
    /// initlalize
    /// </summary>
    /// <param name="managedInventory"></param>
    public void initialize(IInventory managedInventory) {
      if (managedInventory is ShapedPack packInventory) {
        initialize(packInventory, Player.InventoryTypes.GridPack);
      } else throw new ArgumentException("LocalPlayerGridPackController requires an ShapedPack IInventory type");
    }

    /// <summary>
    /// Initialize this inventory
    /// </summary>
    /// <param name="managedInventory"></param>
    /// <param name="inventoryType"></param>
    public override void initialize(ShapedPack managedInventory, Enum inventoryType = null) {
      base.initialize(managedInventory, inventoryType ?? Player.InventoryTypes.GridPack);
      populateShortcutList();
      isInitalized = true;
    }

    /// <summary>
    /// polulate the shortcuts we have
    /// </summary>
    public void populateShortcutList() {
      if (shortcuts != null && shortcuts.Length > 0) {
        foreach (ItemSlotController shortcut in shortcuts) {
          Destroy(shortcut);
        }
        shortcuts = null;
      }
      Item[] tools = Barbelo.LocalPlayerManager.player.hotBarInventory.search(item => item is ITool);
      Dictionary<Tool.Type, ItemSlotController> shortcutControllers = new Dictionary<Tool.Type, ItemSlotController>();
      int shortcutIndex = 0;
      // add the multitool shortcut first:
      shortcutControllers.Add(Tool.Type.Multi, ItemSlotController.Make(
        ShortcutBar,
        ShortcutSlotPrefab,
        (shortcutIndex++, 0),
        Player.InventoryTypes.Shortcuts,
        Tool.Shortcuts[Tool.Type.Multi]
      ));

      // add in any other tool shortcuts we have
      foreach (ITool tool in tools) {
        if (!shortcutControllers.ContainsKey(tool.ToolType)) {
          shortcutControllers.Add(tool.ToolType, ItemSlotController.Make(
            ShortcutBar,
            ShortcutSlotPrefab,
            (shortcutIndex++, 0),
            Player.InventoryTypes.Shortcuts,
            Tool.Shortcuts[tool.ToolType]
          ));
        }
      }

      shortcuts = shortcutControllers.Values.ToArray();
    }

    #endregion

    #region IObserver

    /// <summary>
    /// Receive notifications
    /// </summary>
    /// <param name="event"></param>
    public void notifyOf(IEvent @event) {
      switch (@event) {
        case PlayerCharacterInventoryManager.ItemsUpdatedEvent lpimIUE:
          if (lpimIUE.updatedInventoryType == Player.InventoryTypes.GridPack) {
            foreach (Coordinate updatedItemPivot in lpimIUE.modifiedPivots) {
              updateSlot(updatedItemPivot);
            }
          }
          if (lpimIUE.updatedInventoryType == Player.InventoryTypes.GridPack
            || lpimIUE.updatedInventoryType == Player.InventoryTypes.HotBar 
            || lpimIUE.updatedInventoryType == Player.InventoryTypes.Pockets
          ) {
            populateShortcutList();
          }
          break;
        case PlayerCharacterInventoryManager.ItemsRemovedEvent lpimIRE:
          if (lpimIRE.updatedInventoryType == Player.InventoryTypes.GridPack) {
            foreach (Coordinate updatedItemPivot in lpimIRE.modifiedPivots) {
              updateSlot(updatedItemPivot);
            }
          }
          if (lpimIRE.updatedInventoryType == Player.InventoryTypes.GridPack
            || lpimIRE.updatedInventoryType == Player.InventoryTypes.HotBar
            || lpimIRE.updatedInventoryType == Player.InventoryTypes.Pockets
          ) {
            populateShortcutList();
          }
          break;
        default:
          break;
      }
    }

    #endregion
  }
}