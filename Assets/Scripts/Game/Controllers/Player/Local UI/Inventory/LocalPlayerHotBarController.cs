﻿using SpiritWorld.Events;
using SpiritWorld.Inventories;
using SpiritWorld.Inventories.Items;
using SpiritWorld.Managers;
using SpiritWorld.World.Entities.Creatures;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace SpiritWorld.Game.Controllers {
  [RequireComponent(typeof(VerticalLayoutGroup))]
  public class LocalPlayerHotBarController : MonoBehaviour, ILocalPlayerInventoryController {

    #region Constants

    /// <summary>
    /// Grid item slot prefab
    /// </summary>
    public ItemSlotController BarSlotPrefab;

    /// <summary>
    /// Max distance at which faded items can be seen still
    /// </summary>
    const int MaxVisibleBarSlotDistance
      = 4;

    /// <summary>
    /// The unit height of the item bar per slot
    /// </summary>
    const float ItemBarUnitHeight
      = 100;

    #endregion

    /// <summary>
    /// The canvas for this.
    /// </summary>
    public Canvas canvas
      => _canvas ?? (_canvas = GetComponentInParent<Canvas>());
    Canvas _canvas;

    /// <summary>
    /// The item selected by the hot bar
    /// </summary>
    public Item selectedItem
      => inventory.getItemAt(currentlySelectedItemIndex);

    /// <summary>
    /// The index in the item bar that's currently selected
    /// </summary>
    public int currentlySelectedItemIndex {
      get;
      private set;
    } = 0;

    /// <summary>
    /// The bar item slots
    /// </summary>
    ItemSlotController[] barSlots;

    /// <summary>
    /// The inventoy this is for.
    /// </summary>
    ItemBar inventory;

    /// <summary>
    /// The index in the item bar that's currently hovered over
    /// </summary>
    int currentlyHoveredItemIndex
      = 0;

    /// <summary>
    /// the slot being previewed, if there is one.
    /// </summary>
    int currentPreviewSlotIndex 
      = GridBasedInventory.EmptyGridSlot;

    /// <summary>
    /// This's transform
    /// </summary>
    RectTransform rectTransform
      => _rectTransform ?? (_rectTransform = GetComponent<RectTransform>());
    RectTransform _rectTransform;

    #region Initialization

    /// <summary>
    /// If this has been initalized yet.
    /// </summary>
    bool isInitalized 
      = false;

    /// <summary>
    /// Set up the grid based on player inventory
    /// </summary>
    void Start() {
      if (!isInitalized) {
        initialize(Barbelo.LocalPlayerManager.player.hotBarInventory);
      }
    }

    /// <summary>
    /// instantiate
    /// </summary>
    /// <param name="managedInventory"></param>
    public void initialize(IInventory managedInventory) {
      if (managedInventory is ItemBar barInventory) {
        initialize(barInventory);
      } else throw new ArgumentException("LocalPlayerHotBarController requires an ItemBar IInventory type");
    }

    /// <summary>
    /// instantiate
    /// </summary>
    /// <param name="managedInventory"></param>
    public void initialize(ItemBar managedInventory) {
      inventory = managedInventory;
      populateBarSlots();
    }

    /// <summary>
    /// populate the bar slots from scratch.
    /// </summary>
    void populateBarSlots() {
      resizeBar(inventory.slotCount);
      barSlots = new ItemSlotController[inventory.slotCount];
      for (int slotIndex = 0; slotIndex < inventory.slotCount; slotIndex++) {
        updateSlot(slotIndex);
        /// set 0 as selected
        if (slotIndex == currentlySelectedItemIndex) {
          barSlots[slotIndex].updateMode(ItemSlotController.Mode.Selected);
        }
      }
    }

    #endregion

    #region Slot Manipulation

    /// <summary>
    /// Update the item bar for an item hovered over a location
    /// </summary>
    /// <param name="mousePosition"></param>
    public void updateForItemHoverOver(Vector2 mousePosition) {
      if (tryToGetItemBarHoverPosition(mousePosition, out short barItemSlot, out float slotOffset)
        && !(new Coordinate(barItemSlot, 0).Equals(ItemInventoryDragController.ItemBeingDragged?.originalGridLocation)
          && ItemInventoryDragController.ItemBeingDragged?.parentInventory == Player.InventoryTypes.HotBar)
      ) {
        // if there's currently a hovrred item and it's not this one, we need to shrink the old one it first.
        if (currentlyHoveredItemIndex != GridBasedInventory.EmptyGridSlot && currentlyHoveredItemIndex != barItemSlot) {
          barSlots[currentlyHoveredItemIndex].updateMode(barItemSlot == currentlySelectedItemIndex 
            ? ItemSlotController.Mode.Selected 
            : ItemSlotController.Mode.Default
          );
          currentlyHoveredItemIndex = GridBasedInventory.EmptyGridSlot;
        }
        // if the player is holding in the key to expand the item bar, only works if they have an empty item last in the bar
        if (Input.GetButton("Expand Item Bar") && inventory.getItemAt(inventory.slotCount - 1) == null) {
          int newPreviewSlotIndex = slotOffset > 0.50f ? barItemSlot + 1 : Math.Max(0, (int)barItemSlot);
          // if there's already a preview slot and it's not the current one, reset it.
          if (currentPreviewSlotIndex != newPreviewSlotIndex && currentPreviewSlotIndex != GridBasedInventory.EmptyGridSlot) {
            resetPreviewSlot();
            shiftPreviewSlotTo(newPreviewSlotIndex);
          }
         // if the player isn't holding the key down to expand
        } else {
          if (currentPreviewSlotIndex != GridBasedInventory.EmptyGridSlot) {
            resetPreviewSlot();
          }
          // if this isn't alrready the hovered item, set it as such.
          if (barSlots[barItemSlot].currentMode != ItemSlotController.Mode.Hovered) {
            currentlyHoveredItemIndex = barItemSlot;
            barSlots[currentlyHoveredItemIndex].updateMode(ItemSlotController.Mode.Hovered);
          }
        }
      } else {        
        // if there's currently a hovrred item and it's not this one, we need to shrink the old one
        if (currentlyHoveredItemIndex != GridBasedInventory.EmptyGridSlot) {
          barSlots[currentlyHoveredItemIndex].updateMode(barItemSlot == currentlySelectedItemIndex
            ? ItemSlotController.Mode.Selected
            : ItemSlotController.Mode.Default
          );
          currentlyHoveredItemIndex = GridBasedInventory.EmptyGridSlot;
        }
        // if there's currently a previewed item and we've moved away, reset the slots.
        if (currentPreviewSlotIndex != GridBasedInventory.EmptyGridSlot) {
          resetPreviewSlot();
        }
      }
    }

    /// <summary>
    /// Update the slot
    /// </summary>
    /// <param name="slotIndex"></param>
    public void updateSlot(Coordinate slotIndex) {
      updateSlot(slotIndex.x);
    }

    /// <summary>
    /// reset the slot being used as a preview if there's one.
    /// </summary>
    public void resetPreviewSlot() {
      if (currentPreviewSlotIndex != GridBasedInventory.EmptyGridSlot) {
        barSlots[inventory.slotCount - 1].transform.SetSiblingIndex(inventory.slotCount - 1);
        barSlots[inventory.slotCount - 1].updateMode(currentPreviewSlotIndex == currentlySelectedItemIndex
          ? ItemSlotController.Mode.Selected
          : ItemSlotController.Mode.Default
        );
        currentPreviewSlotIndex = GridBasedInventory.EmptyGridSlot;
      }
    }

    /// <summary>
    /// Update the item at the given slot, or empty it
    /// </summary>
    /// <param name="slotIndex"></param>
    /// <param name="pushDown"></param>
    void updateSlot(int slotIndex) {
      if (barSlots[slotIndex] != null) {
        barSlots[slotIndex].updateItemIcon(inventory.getItemAt(slotIndex));
      } else {
        barSlots[slotIndex] = ItemSlotController.Make(
          transform,
          BarSlotPrefab,
          (slotIndex, 0),
          Player.InventoryTypes.HotBar,
          inventory.getItemAt(slotIndex)
        );
      }
      barSlots[slotIndex].updateOpacity(getAlphaForSlot(slotIndex));
      if (slotIndex == currentlySelectedItemIndex) {
        barSlots[slotIndex].updateMode(ItemSlotController.Mode.Selected);
      }
    }

    /// <summary>
    /// Move the bottom empty slot of the bar to the given slot as an empty preview.
    /// </summary>
    /// <param name="previewSlotIndex"></param>
    void shiftPreviewSlotTo(int previewSlotIndex) {
      currentPreviewSlotIndex = previewSlotIndex;
      barSlots[inventory.slotCount - 1].transform.SetSiblingIndex(previewSlotIndex);
      barSlots[inventory.slotCount - 1].updateMode(ItemSlotController.Mode.Hovered);
    }

    /// <summary>
    /// Get alpha for a slot
    /// </summary>
    /// <param name="slotIndex"></param>
    /// <returns></returns>
    float getAlphaForSlot(int slotIndex) {
      float distanceFromSelectedItem = Math.Abs(slotIndex - currentlySelectedItemIndex);
      return distanceFromSelectedItem >= MaxVisibleBarSlotDistance
        ? 0
        : 1f - (float)distanceFromSelectedItem / MaxVisibleBarSlotDistance;
    }

    #endregion

    #region Bar Maniupulation

    // Update is called once per frame
    void Update() {
      scroll();
    }

    /// <summary>
    /// Check if we should scroll and do so
    /// </summary>
    void scroll() {
      if (!ItemInventoryDragController.AnItemIsBeingDragged && !Input.GetButton("Rotate Camera Lock")) {
        float scrollDelta = Input.GetAxis("Mouse ScrollWheel");
        int previousSelectedItemIndex = currentlySelectedItemIndex;
        // data value change and actual movement of the bar
        if (scrollDelta > 0) {
          tryToScrollUp();
        } else if (scrollDelta < 0) {
          tryToScrollDown();
        }

        /// visual changes to item slots
        if (currentlySelectedItemIndex != previousSelectedItemIndex) {
          int itemSlotControllerIndex = 0;
          foreach (ItemSlotController barSlot in barSlots) {
            // size
            if (itemSlotControllerIndex == currentlySelectedItemIndex) {
              barSlot.updateMode(ItemSlotController.Mode.Selected);
            } else if (barSlot.currentMode != ItemSlotController.Mode.Default) {
              barSlot.updateMode(ItemSlotController.Mode.Default);
            }

            // fade
            barSlot.updateOpacity(getAlphaForSlot(itemSlotControllerIndex++));
          }
        }
      }
    }

    /// <summary>
    /// Try scrolling the bar down
    /// </summary>
    void tryToScrollDown() {
      if (currentlySelectedItemIndex > 0) {
        currentlySelectedItemIndex--;
        rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, rectTransform.anchoredPosition.y - ItemBarUnitHeight);
      }
    }

    /// <summary>
    /// try scrolling the bar up
    /// </summary>
    void tryToScrollUp() {
      if (currentlySelectedItemIndex < inventory.slotCount - 1) {
        currentlySelectedItemIndex++;
        rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, rectTransform.anchoredPosition.y + ItemBarUnitHeight);
      }
    }

    /// <summary>
    /// resize the item bar
    /// </summary>
    /// <param name="newSlotCount"></param>
    void resizeBar(int newSlotCount) {
      rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, newSlotCount * ItemBarUnitHeight);
    }

    #endregion

    /// <summary>
    /// try to get the item bar slot we're hovering over
    /// TODO: test this
    /// </summary>
    /// <returns></returns>
    public bool tryToGetItemBarHoverPosition(Vector2 pointerPosition, out short barItemSlot, out float slotOffset) {
      if (RectTransformUtility.ScreenPointToLocalPointInRectangle(
         rectTransform,
         pointerPosition,
         canvas.worldCamera,
         out Vector2 localCursor
       ) && rectTransform.rect.Contains(localCursor)) {
        int barSlot = -(int)(localCursor.y / ItemBarUnitHeight);
        if (barSlot >= 0 && barSlot < inventory.slotCount) {
          slotOffset = localCursor.y / ItemBarUnitHeight - barSlot;

          barItemSlot = (short)barSlot;
          return true;
        }
      }

      barItemSlot = 0;
      slotOffset = 0;
      return false;
    }


    #region IObserver

    /// <summary>
    /// Receive notifications
    /// </summary>
    /// <param name="event"></param>
    public void notifyOf(IEvent @event) {
      switch (@event) {
        case PlayerCharacterInventoryManager.ItemsUpdatedEvent lpimIUE:
          if (lpimIUE.updatedInventoryType == Player.InventoryTypes.HotBar) {
            foreach (Coordinate updatedItemPivot in lpimIUE.modifiedPivots) {
              updateSlot(updatedItemPivot);
            }
          }
          break;
        case PlayerCharacterInventoryManager.ItemsRemovedEvent lpimIRE:
          if (lpimIRE.updatedInventoryType == Player.InventoryTypes.HotBar) {
            foreach (Coordinate updatedItemPivot in lpimIRE.modifiedPivots) {
              updateSlot(updatedItemPivot);
            }
          }
          break;
        default:
          break;
      }
    }

    #endregion
  }
}
