﻿using SpiritWorld.Events;
using SpiritWorld.Inventories;
using SpiritWorld.Managers;
using SpiritWorld.World.Entities.Creatures;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SpiritWorld.Game.Controllers {

  /// <summary>
  /// Controls the local player's pocket inventory menu ui
  /// </summary>
  public class LocalPlayerPocketsController : MonoBehaviour, ILocalPlayerInventoryController {

    #region Constants

    /// <summary>
    /// The pocket item slot prefab
    /// </summary>
    public ItemSlotController PocketSlotPrefab;

    #endregion

    /// <summary>
    /// The canvas for this.
    /// </summary>
    public Canvas canvas
      => _canvas ?? (_canvas = GetComponentInParent<Canvas>());
    Canvas _canvas;

    /// <summary>
    /// This's transform
    /// </summary>
    RectTransform rectTransform
      => _rectTransform ?? (_rectTransform = GetComponent<RectTransform>());
    RectTransform _rectTransform;

    /// <summary>
    /// the pocket slots
    /// </summary>
    Transform[] pocketSlotsLocations;

    /// <summary>
    /// the pocket slots
    /// </summary>
    ItemSlotController[] pocketSlots;

    /// <summary>
    /// The inventoy this is for.
    /// </summary>
    ItemBar inventory;

    #region Initialization

    /// <summary>
    /// If this has been initalized yet.
    /// </summary>
    bool isInitalized = false;

    /// <summary>
    /// Set up the grid based on player inventory
    /// </summary>
    void Start() {
      if (!isInitalized) {
        initialize(Barbelo.LocalPlayerManager.player.pocketInventory);
      }
    }

    /// <summary>
    /// instantiate
    /// </summary>
    /// <param name="managedInventory"></param>
    public void initialize(IInventory managedInventory) {
      if (managedInventory is ItemBar pocketInventory) {
        inventory = pocketInventory;
        List<Transform> pocketSlotTransforms = new List<Transform>();
        foreach(Transform pocketSlotTransform in transform) {
          pocketSlotTransforms.Add(pocketSlotTransform);
        }
        pocketSlotsLocations = pocketSlotTransforms.ToArray();
        populatePocketSlots();
      } else throw new ArgumentException("LocalPlayerPocketsController requires an ItemBar IInventory type");
    }

    /// <summary>
    /// populate the pocket slots with the current inventory from scratch.
    /// </summary>
    void populatePocketSlots() {
      pocketSlots = new ItemSlotController[inventory.slotCount];
      for (int slotIndex = 0; slotIndex < inventory.slotCount; slotIndex++) {
        pocketSlotsLocations[slotIndex].gameObject.SetActive(true);
        updateSlot(slotIndex);
      }
    }

    #endregion

    #region Slot Modifications

    /// <summary>
    /// Update the slot
    /// </summary>
    /// <param name="slotIndex"></param>
    public void updateSlot(Coordinate slotIndex) {
      updateSlot(slotIndex.x);
    }

    /// <summary>
    /// Update what item is displayed in the current slot.
    /// </summary>
    /// <param name="slotIndex"></param>
    public void updateSlot(int slotIndex) {
      if (slotIndex < inventory.slotCount) {
        // TODO use updateSlotIndex instead of create/destroying it.
        if (pocketSlots[slotIndex] != null) {
          Destroy(pocketSlots[slotIndex].gameObject);
        }
        pocketSlots[slotIndex] = ItemSlotController.Make(
          pocketSlotsLocations[slotIndex],
          PocketSlotPrefab,
          (slotIndex, 0),
          Player.InventoryTypes.Pockets,
          inventory.getItemAt(slotIndex)
        );
      }
    }

    #endregion

    /// <summary>
    /// Get the closest pocket slot to the given drop location
    /// </summary>
    public bool tryToGetClosestPocketSlot(Vector2 clickLocation, out Coordinate closestSlot) {
      int closestSlotId = GridBasedInventory.EmptyGridSlot;
      if (RectTransformUtility.ScreenPointToLocalPointInRectangle(
          rectTransform,
          clickLocation,
          canvas.worldCamera,
          out Vector2 localPoint
      ) && rectTransform.rect.Contains(localPoint)) {
        float closestSlotDistance = float.MaxValue;
        foreach (ItemSlotController slot in pocketSlots) {
          Vector2 slotScreenLocation = RectTransformUtility.WorldToScreenPoint(canvas.worldCamera, slot.transform.position);
          float distanceFromSlot = Vector2.Distance(slotScreenLocation, clickLocation);
          if (distanceFromSlot < closestSlotDistance) {
            closestSlotDistance = distanceFromSlot;
            closestSlotId = slot.slotIndex;
          }
        }
      }

      closestSlot = (closestSlotId, 0);
      return closestSlotId != GridBasedInventory.EmptyGridSlot;
    }


    #region IObserver

    /// <summary>
    /// Receive notifications
    /// </summary>
    /// <param name="event"></param>
    public void notifyOf(IEvent @event) {
      switch (@event) {
        case PlayerCharacterInventoryManager.ItemsUpdatedEvent lpimIUE:
          if (lpimIUE.updatedInventoryType == Player.InventoryTypes.Pockets) {
            foreach (Coordinate updatedItemPivot in lpimIUE.modifiedPivots) {
              updateSlot(updatedItemPivot);
            }
          }
          break;
        case PlayerCharacterInventoryManager.ItemsRemovedEvent lpimIRE:
          if (lpimIRE.updatedInventoryType == Player.InventoryTypes.Pockets) {
            foreach (Coordinate updatedItemPivot in lpimIRE.modifiedPivots) {
              updateSlot(updatedItemPivot);
            }
          }
          break;
        default:
          break;
      }
    }

    #endregion
  }
}