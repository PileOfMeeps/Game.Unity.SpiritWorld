﻿using SpiritWorld.Inventories.Items;
using SpiritWorld.World.Entities.Creatures;
using System;
using UnityEngine;

namespace SpiritWorld.Game.Controllers {

  /// <summary>
  /// The controller for the player ui
  /// </summary>
  public class PlayerInventoryUIController : MonoBehaviour {

    #region Constants

    /// <summary>
    /// The hot bar controller
    /// SET IN UNITY
    /// </summary>
    public LocalPlayerHotBarController HotBarController;

    /// <summary>
    /// The grid pack controller
    /// SET IN UNITY
    /// </summary>
    public LocalPlayerGridPackController GridPackController;

    /// <summary>
    /// The pocket controller
    /// SET IN UNITY
    /// </summary>
    public LocalPlayerPocketsController PocketsController;

    #endregion

    /// <summary>
    /// If the item pack menu is currently open and visible
    /// </summary>
    public bool packMenuIsOpen
      => GridPackController.ItemPackMenuController.menuIsVisible;

    /// <summary>
    /// The selected item for the local user
    /// </summary>
    public Item selectedItem
      => HotBarController.selectedItem;

    /// <summary>
    /// Get the controller for the inventory type
    /// </summary>
    /// <param name="inventoryType"></param>
    /// <returns></returns>
    public ILocalPlayerInventoryController getControllerFor(Player.InventoryTypes inventoryType) {
      return inventoryType switch {
        Player.InventoryTypes.GridPack => GridPackController,
        Player.InventoryTypes.Shortcuts => GridPackController,
        Player.InventoryTypes.HotBar => HotBarController,
        Player.InventoryTypes.Pockets => PocketsController,
        _ => throw new ArgumentException(inventoryType + " does not have a valid ILocalPlayerInventoryController"),
      };
    }
  }
}
