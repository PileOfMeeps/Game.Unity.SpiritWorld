﻿using SpiritWorld.Events;
using SpiritWorld.Inventories.Items;
using SpiritWorld.World.Terrain.Features;
using SpiritWorld.World.Terrain.TileGrid;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SpiritWorld.Managers;

namespace SpiritWorld.Game.Controllers {
  public class TileSelectionController : MonoBehaviour, IObserver {

    /// <summary>
    /// The camera used to show the UI
    /// </summary>
    public Camera UICamera;

    /// <summary>
    /// The minimum time a key must be held down in order to get a hold action instead of a click action
    /// </summary>
    const float MinimumHoldDownTime = 0.3f;

    /// <summary>
    /// The object used to hilight the selected tile
    /// </summary>
    public GameObject selectedTileHilight;

    /// <summary>
    /// The name/type text for the investigated tile
    /// </summary>
    public Text investigatedTileNameText;

    /// <summary>
    /// The position (x.y.z) text for the investigated tile
    /// </summary>
    public Text investigatedTilePositionText;

    /// <summary>
    /// The tile features text for the investigated tile
    /// </summary>
    public Text investigatedTileFeaturesText;

    /// <summary>
    /// The gameobject to hide and unhide used for the "working on tile ui"
    /// </summary>
    public GameObject workOnTileIndicator;

    /// <summary>
    /// The progress circle image for the work on tile indicator
    /// </summary>
    public Image workOnTileProgressCircle;

    /// <summary>
    /// The tool type icon for the work on tile indicator
    /// </summary>
    public Image workOnTileToolTypeIndicator;

    /// <summary>
    /// The currently selected tile
    /// </summary>
    public Tile selectedTile {
      get;
      private set;
    }

    /// <summary>
    /// The features for the selected tile
    /// </summary>
    public FeaturesByLayer selectedTileFeatures
      => Barbelo.ActiveScapeManager.activeBoard.getFeaturesFor(selectedTile);

    /// <summary>
    /// A timer for performing an action
    /// </summary>
    float actionTimer = 0.0f;

    // Update is called once per frame
    void Update() {
      selectHoveredTile();
      tryToActOnSelectedTile();
    }

    /// <summary>
    /// Select and hilight the tile the mouse is hovering over
    /// </summary>
    void selectHoveredTile() {
      // we only want to change the selected tile when we're not acting on a tile atm.
      //Also select tiles if we're dragging an item
      // TODO add a way to check if we're holding an item over an inventory vs a tile.
      if (!Input.GetButton("Act") || (ItemInventoryDragController.AnItemIsBeingDragged/* && !ItemIsHoveredOverInventoryBackgroundOfSomeKind*/)) {
        /// if the mouse is pointing at the tileboard, use a ray get data about the tile
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(ray.origin, ray.direction * 25, Color.red);
        if (Physics.Raycast(ray, out RaycastHit hit, 25) && hit.collider.gameObject.CompareTag("TileBoard")) {
          // get the place we hit, plus a little forward in case we hit the side of a column
          Vector3 gridHitPosition = hit.point + (hit.normal * 0.1f);
          // zero out the y, we dont' account for height in the coords.
          gridHitPosition.y = 0;
          // get which tile we hit and the chunk it's in
          selectedTile = Barbelo.ActiveScapeManager.activeBoard.get(gridHitPosition);

          /// Move the selected tile hilight to the correct location
          if (selectedTileHilight != null) {
            selectedTileHilight.transform.position = new Vector3(
              selectedTile.worldLocation.x,
              selectedTile.height * Universe.StepHeight,
              selectedTile.worldLocation.z
            );
          }
        }
      }
    }

    /// <summary>
    /// Act on the selected tile when appropriate
    /// </summary>
    void tryToActOnSelectedTile() {
      if (!ItemInventoryDragController.AnItemIsBeingDragged) {
        /// if we just pressed the button, investigate the newly selected tile
        if (Input.GetButtonDown("Act")) {
          investigate(selectedTile);
        }
        // if we're holding the button
        if (Input.GetButton("Act")) {
          actionTimer += Time.deltaTime;
          // if we've been holding it for the minimum hold time at least, act via hold action
          if (actionTimer >= MinimumHoldDownTime) {
            holdDownActionOnSelectedTile();
          }
        }
      }
      // if we've let go of the button
      if (Input.GetButtonUp("Act")) {
        // if we were holding an item and release it on a tile, check if that does something special:
        if (ItemInventoryDragController.AnItemIsBeingDragged) {
          // TODO: try to use the item on the tile
        } else {
          // if we haven't gone over minimum hold time, do the single click action on the tile
          if (actionTimer < MinimumHoldDownTime) {
            shortClickActionOnSelectedTile();
          }
          // reset the action timer and progress wheel
          actionTimer = 0;
          workOnTileIndicator.SetActive(false);
        }
      }
    }

    /// <summary>
    /// Use an equiped tool on the tile's feature
    /// </summary>
    /// <param name="actionTimer"></param>
    void holdDownActionOnSelectedTile() {
      ITool selectedTool = Saturn.LocalPlayerController.InventoryUIController.HotBarController.selectedItem as ITool;
      FeaturesByLayer features = Barbelo.ActiveScapeManager.activeBoard.getFeaturesFor(selectedTile);
      Barbelo.ActiveScapeManager.useItemOnTile(
        Saturn.LocalPlayerController.player,
        Saturn.LocalPlayerController.InventoryUIController.HotBarController.currentlySelectedItemIndex,
        selectedTile,
        actionTimer
      );
      /// update the progress bar locally
      if (features != null
        && features.TryGetValue(TileFeature.Layer.Resource, out TileFeature resource)
        && resource.type.CanBeMinedBy(selectedTool, resource.mode)
      ) {
        // if we have features that can be mined by the selected tool, increase the timer
        updateTileProgressBar(resource.remainingInteractions == 0 || !resource.type.CanBeMinedBy(selectedTool, resource.mode)
          ? 0
          : actionTimer,
          resource.type.TimeToUse
        );

        // set the info to the updated tile.
        investigate(selectedTile);
      } else if (selectedTool.ToolType == Tool.Type.Shovel) {
        if (features == null || features.Count == 0) {
          // TODO: if we can shovel the empty tile

        } else if (features.TryGetValue(TileFeature.Layer.Decoration, out TileFeature decoration) && decoration.mode == 0) {
          // if we can shovel the tile decoration
          updateTileProgressBar(0, decoration.type.TimeToUse);
        }
      } else {
        // if not hide it.
        updateTileProgressBar(0, 1);
        workOnTileIndicator.SetActive(false);
      }
    }

    /// <summary>
    /// display info about the tile
    /// </summary>
    void shortClickActionOnSelectedTile() {
      // empty for now
    }

    /// <summary>
    /// Investigate the given tile for it's info, and display it
    /// </summary>
    /// <param name="tile"></param>
    void investigate(Tile tile) {
      if (tile.type != null) {
        investigatedTileNameText.text = tile.type.Name;
        investigatedTilePositionText.text = $"({tile.axialKey.x}, {tile.height}, {tile.axialKey.z})";

        /// if we have features, name them.
        string featureText;
        FeaturesByLayer features = selectedTileFeatures;
        if (features != null) {
          featureText = "Features:\n";
          foreach (KeyValuePair<TileFeature.Layer, TileFeature> feature in features) {
            featureText += $"{feature.Value.type.Name} {(feature.Value.type.IsInteractive ? $"({feature.Value.remainingInteractions}/{feature.Value.type.NumberOfUses})" : "")}\n";
          }
        } else {
          featureText = "Empty";
        }
        investigatedTileFeaturesText.text = featureText;
      }
    }

    /// <summary>
    /// Update the tile progress bar with time remaining
    /// </summary>
    /// <param name="holdTimeSoFar"></param>
    /// <param name="useTimeofFeature"></param>
    void updateTileProgressBar(float holdTimeSoFar, float useTimeofFeature) {
      // hide it unless we're holding the bar in
      if (holdTimeSoFar > MinimumHoldDownTime) {
        if (workOnTileIndicator.activeSelf == false) {
          workOnTileIndicator.SetActive(true);
        }
        float wheelPercentage = holdTimeSoFar / useTimeofFeature;
        workOnTileProgressCircle.fillAmount = wheelPercentage;
        workOnTileIndicator.transform.position = UICamera.ScreenToWorldPoint(Input.mousePosition);
      } else if (workOnTileIndicator.activeSelf == true) {
        workOnTileIndicator.SetActive(false);
      }
    }

    #region IObserver

    /// <summary>
    /// Listen for the tile we're working on to be updated
    /// </summary>
    /// <param name="event"></param>
    public void notifyOf(IEvent @event) {
      switch(@event) {
        // if it's the local player who updated a tile, reset the tile update timer for this local player
        case ScapeManager.TileFeatureUpdated smTFU:
          if (smTFU.player == Saturn.LocalPlayerController.player) {
            actionTimer = 0;
          }
          break;
        case ScapeManager.TileFeatureDestroyed smTFD:
          if (smTFD.player == Saturn.LocalPlayerController.player) {
            actionTimer = 0;
            workOnTileIndicator.SetActive(false);
          }
          break;
        default:
          break;
      }
    }

    #endregion
  }
}
