﻿using SpiritWorld.Inventories.Items;
using SpiritWorld.World.Entities.Creatures;
using UnityEngine;

namespace SpiritWorld.Game.Controllers {

  /// <summary>
  /// The local player controller
  /// </summary>
  [RequireComponent(typeof(PlayerInventoryUIController))]
  [RequireComponent(typeof(LocalPlayerMotionController))]
  public class PlayerController : MonoBehaviour {

    #region Constants

    ///// SET VIA UNITY

    /// <summary>
    /// The local player's notification manager
    /// </summary>
    public NotificationsUIController NotificationsUIController;

    /// <summary>
    /// Manages tile selection for the local player
    /// </summary>
    public TileSelectionController TileSelectionController;

    /////////////////////

    /// <summary>
    /// The movement controller for the local player
    /// </summary>
    public LocalPlayerMotionController MotionController
      => _MotionController ?? (_MotionController = GetComponent<LocalPlayerMotionController>());
    LocalPlayerMotionController _MotionController;

    /// <summary>
    /// The local player's inventory manager.
    /// </summary>
    public PlayerInventoryUIController InventoryUIController
      => _InventoryController ?? (_InventoryController = GetComponent<PlayerInventoryUIController>());
    PlayerInventoryUIController _InventoryController;

    #endregion

    /// <summary>
    /// The currently selected tool of the user
    /// TODO: move this functionality to ItemHotBarController.selectedItem for the local player only
    /// </summary>
    public ITool selectedTool {
      get;
      protected set;
    } = Player.EmptyHand;

    /// <summary>
    /// The player being controled
    /// </summary>
    public Player player {
      get;
      protected set;
    }

    /// <summary>
    /// Initialize
    /// </summary>
    /// <param name="player"></param>
    public void initialize(Player player) {
      this.player = player;

      // subscribe this to all channels for notifications
      Barbelo.EventSystem.subscribeToAll(NotificationsUIController);

      // listen for inventory changes
      Barbelo.EventSystem.subscribe(
        InventoryUIController.GridPackController,
        Events.WorldScapeEventSystem.Channels.LocalPlayerUpdates
      );
      Barbelo.EventSystem.subscribe(
        InventoryUIController.HotBarController,
        Events.WorldScapeEventSystem.Channels.LocalPlayerUpdates
      );
      Barbelo.EventSystem.subscribe(
        InventoryUIController.PocketsController,
        Events.WorldScapeEventSystem.Channels.LocalPlayerUpdates
      );

      // listen for tile updates
      Barbelo.EventSystem.subscribe(
        TileSelectionController,
        Events.WorldScapeEventSystem.Channels.TileUpdates
      );
    }
  }
}