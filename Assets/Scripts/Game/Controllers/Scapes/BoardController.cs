﻿using Mirror;
using SpiritWorld.Events;
using SpiritWorld.Inventories.Items;
using SpiritWorld.World.Terrain.Features;
using SpiritWorld.World.Terrain.TileGrid;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace SpiritWorld.Game.Controllers {

  /// <summary>
  /// Manages a board made up of tilegrid chunks in the overworld scape
  /// TODO: This is the board controller
  /// </summary>
  public abstract class BoardController : MonoBehaviour, IObserver {

    #region Constants

    /// <summary>
    /// How often to update the active chunks in seconds
    /// SET IN UNITY
    /// TODO: Finalize and remove from unity
    /// </summary>
    public float ChunkUpdateTickSeconds = 1;

    /// <summary>
    /// The chunk controllers we have available to use
    /// PLACED IN UNITY
    /// </summary>
    ChunkController[] ChunkControllerPool
      => _chunkControllerPool ?? (_chunkControllerPool = gameObject.GetComponentsInChildren<ChunkController>(true));
    ChunkController[] _chunkControllerPool;

    #endregion

    /// <summary>
    /// The tile board that is currently acitve
    /// </summary>
    protected TileBoard activeBoard {
      get;
      private set;
    }

    /// <summary>
    /// The tick timer
    /// </summary>
    float tickTimer = 0;

    /// <summary>
    /// Controllers currently being used, indexed by which chunk they're being used for
    /// </summary>
    Dictionary<Coordinate, ChunkController> inUseControllers
      = new Dictionary<Coordinate, ChunkController>();

    #region Initialization

    /// <summary>
    /// Set the active board this tileboard is managing.
    /// </summary>
    /// <param name="newBoard"></param>
    [Client]
    public virtual void initialize(TileBoard newBoard) {
      clear();
      activeBoard = newBoard;
      activeBoard.populate((-10, -10), (11, 11));
      loadBoardAround(Saturn.LocalPlayerController.transform.position);
    }

    /// <summary>
    /// Load the board around a given tile's X/Z in the world.
    /// </summary>
    /// <param name="worldLocation"></param>
    [Client]
    public void loadBoardAround(Coordinate worldLocation) {
      if (activeBoard != null) {
        /// figure out which board chunks we want to load and load them
        // collect the locations of the board chunks we need to load based on the centered world location
        Coordinate[] chunkLocationsToLoad = getLiveChunksAround(worldLocation);

        // assign controllers to the board chunks
        int index = 0;
        foreach (Coordinate chunkLocationKey in chunkLocationsToLoad) {
          // currently chunks load on assignment
          inUseControllers[chunkLocationKey] = ChunkControllerPool[index];
          ChunkControllerPool[index++].updateGridTo(activeBoard[chunkLocationKey], chunkLocationKey);
        }
      }
    }

    /// <summary>
    /// Get the board locations of the chunks that should be visible to a player at the given worldlocation
    /// </summary>
    /// <returns>The board locations (x/z of the grid in the tileboard)</returns>
    protected abstract Coordinate[] getLiveChunksAround(Vector3 worldLocation);

    /// <summary>
    /// Get an empty controller index
    /// </summary>
    /// <returns></returns>
    int getUnusedControllerIndex() {
      int index = 0;
      foreach (ChunkController controller in ChunkControllerPool) {
        if (!controller.isInUse) {
          return index;
        }
        index++;
      }

      return -1;
    }

    #endregion

    #region Chunk Updates

    /// <summary>
    /// Poll player location
    /// </summary>
    void Update() {
      if (tickTimer >= ChunkUpdateTickSeconds) {
        checkAndUpdateChunksAroundLocalPlayer();
        tickTimer = 0;
      } else {
        tickTimer += Time.deltaTime;
      }
    }

    /// <summary>
    /// used for polling player location and updating the loaded chunks
    /// </summary>
    [Client]
    void checkAndUpdateChunksAroundLocalPlayer() {
      Coordinate[] chunkLocationsThatShouldBeLoaded = getLiveChunksAround(Saturn.LocalPlayerController.transform.position);
      // if the chunks that should be loaded don't match the current in use chunk controllers
      IEnumerable<Coordinate> chunksToLoad = chunkLocationsThatShouldBeLoaded.Except(inUseControllers.Keys.ToArray());
      IEnumerable<Coordinate> chunksToUnload = inUseControllers.Keys.ToArray().Except(chunkLocationsThatShouldBeLoaded);
      foreach (Coordinate chunkKey in chunksToUnload) {
        inUseControllers[chunkKey].clear();
        inUseControllers.Remove(chunkKey);
      }
      foreach (Coordinate chunkKeyToLoad in chunksToLoad) {
        // currently chunks load on assignment
        inUseControllers[chunkKeyToLoad] = ChunkControllerPool[getUnusedControllerIndex()];
        inUseControllers[chunkKeyToLoad].updateGridTo(activeBoard[chunkKeyToLoad], chunkKeyToLoad);
      }
    }

    /// <summary>
    /// clear the active board and the controllers being used of their chunks
    /// </summary>
    [Client]
    void clear() {
      activeBoard = null;
      foreach (ChunkController controller in ChunkControllerPool) {
        controller.clear();
      }
    }

    #endregion

    #region Tile Updates

    /// <summary>
    /// Update the mode for a feature on a given tile.
    /// It needs to be on the same layer as the one being updated.
    /// </summary>
    /// <param name="tile"></param>
    /// <param name="featureLayer"></param>
    [Client]
    void updateFeature(Tile tile, TileFeature feature) {
      if (inUseControllers.TryGetValue(tile.parentChunkKey, out ChunkController chunkController)) {
        chunkController.updateFeatureModel(tile, feature);
      }
    }

    /// <summary>
    /// Destroy the feature for the given tile and layer
    /// </summary>
    [Client]
    void destroyFeature(Tile tile, TileFeature.Layer featureLayerToRemove) {
      if (inUseControllers.TryGetValue(tile.parentChunkKey, out ChunkController chunkController)) {
        chunkController.removeFeature(tile, featureLayerToRemove);
      }
    }

    /// <summary>
    /// Instantiate item drops on a tile
    /// </summary>
    [Client]
    void dropItemsOnTile(Tile tile, Item[] drops, int[] dropIds) {
      if (inUseControllers.TryGetValue(tile.parentChunkKey, out ChunkController chunkController)) {
        chunkController.dropItems(tile, drops, dropIds);
      }
    }

    /// <summary>
    /// Remove the drop from the chunk
    /// </summary>
    /// <param name="chunkDropData"></param>
    [Client]
    void removeDrop((int dropId, Coordinate chunkAxialKey) chunkDropData) {
      if (inUseControllers.TryGetValue(chunkDropData.chunkAxialKey, out ChunkController chunkController)) {
        chunkController.removeDrop(chunkDropData.dropId);
      }
    }

    #endregion

    #region IObserver

    /// <summary>
    /// Listen for events
    /// </summary>
    /// <param name="event"></param>
    public void notifyOf(IEvent @event) {
      switch (@event) {
        case Managers.ScapeManager.TileFeatureUpdated tsmTFU:
          updateFeature(tsmTFU.updatedTile, tsmTFU.updatedFeature);
          break;
        case Managers.ScapeManager.TileFeatureDestroyed tsmTFD:
          destroyFeature(tsmTFD.updatedTile, tsmTFD.removedLayer);
          break;
        case Managers.ScapeManager.TileFeatureDropsLeftover tsmTFDL:
          dropItemsOnTile(tsmTFDL.updatedTile, tsmTFDL.drops, tsmTFDL.dropIds);
          break;
        case Managers.InventoryManager.DropItemsEvent pimDI:
          dropItemsOnTile(activeBoard.get(pimDI.playerWorldLocation), pimDI.items, pimDI.dropIds);
          break;
        case ItemDropController.PickedUpEvent idcPUE:
          removeDrop(idcPUE.chunkDropData);
          break;
        default:
          break;
      }
    }

    #endregion
  }
}
