﻿using SpiritWorld.World.Terrain.TileGrid;
using System.Collections.Generic;
using UnityEngine;

namespace SpiritWorld.Game.Controllers {

  /// <summary>
  /// A manager for a rectangular shaped board
  /// </summary>
  public class RectangularBoardController : BoardController {

    /// <summary>
    /// Get the board locations of the chunks that should be visible to a player at the given worldlocation
    /// </summary>
    /// <returns>The board locations (x/z of the grid in the tileboard)</returns>
    protected override Coordinate[] getLiveChunksAround(Vector3 worldLocation) {
      Tile currentTile = activeBoard.get(worldLocation);
      Coordinate currentChunkLocationKey = activeBoard.getChunkKeyFor(currentTile);
      Vector3 inChunkTileLocation = currentTile.localLocation;

      // set up and add the current chunk location
      List<Coordinate> chunkLocationsToLoad = new List<Coordinate> {
        currentChunkLocationKey
      };

      // if we're close to the east of the chunk
      if (inChunkTileLocation.x > RectangularBoard.ChunkWorldCenter.x) {
        chunkLocationsToLoad.Add(currentChunkLocationKey + Directions.East.Offset);
        // if we're also close to the north of the chunk
        if (inChunkTileLocation.z > RectangularBoard.ChunkWorldCenter.z) {
          chunkLocationsToLoad.Add(currentChunkLocationKey + Directions.North.Offset);
          chunkLocationsToLoad.Add(currentChunkLocationKey + Directions.North.Offset + Directions.East.Offset);
          // if we're also close to the south of the chunk
        } else if (inChunkTileLocation.z < RectangularBoard.ChunkWorldCenter.z) {
          chunkLocationsToLoad.Add(currentChunkLocationKey + Directions.South.Offset);
          chunkLocationsToLoad.Add(currentChunkLocationKey + Directions.South.Offset + Directions.East.Offset);
        }
        // if we're close to the west of the chunk
      } else if (inChunkTileLocation.x < RectangularBoard.ChunkWorldCenter.x) {
        chunkLocationsToLoad.Add(currentChunkLocationKey + Directions.West.Offset);
        // if we're also close to the north of the chunk
        if (inChunkTileLocation.z > RectangularBoard.ChunkWorldCenter.z) {
          chunkLocationsToLoad.Add(currentChunkLocationKey + Directions.North.Offset);
          chunkLocationsToLoad.Add(currentChunkLocationKey + Directions.North.Offset + Directions.West.Offset);
          // if we're also close to the south of the chunk
        } else if (inChunkTileLocation.z < RectangularBoard.ChunkWorldCenter.z) {
          chunkLocationsToLoad.Add(currentChunkLocationKey + Directions.South.Offset);
          chunkLocationsToLoad.Add(currentChunkLocationKey + Directions.South.Offset + Directions.West.Offset);
        }
        // if we're only close to the north of the chunk
      } else if (inChunkTileLocation.z > RectangularBoard.ChunkWorldCenter.z) {
        chunkLocationsToLoad.Add(currentChunkLocationKey + Directions.North.Offset);
        // if we're only close to the south of the chunk
      } else if (inChunkTileLocation.z < RectangularBoard.ChunkWorldCenter.z) {
        chunkLocationsToLoad.Add(currentChunkLocationKey + Directions.South.Offset);
      }

      return chunkLocationsToLoad.ToArray();
    }
  }
}
