﻿using Mirror;
using SpiritWorld.Game.Data;
using SpiritWorld.Managers;
using UnityEngine;

namespace SpiritWorld.Game.Controllers {

  /// <summary>
  /// Controls what scape is active on the client
  /// </summary>
  public class ScapeController : MonoBehaviour {

    #region Constants

    /// <summary>
    /// The controller of the boards for this worldscape
    /// SET IN UNITY
    /// </summary>
    public BoardController BoardController;

    #endregion

    #region Initialization

    /// <summary>
    /// Initialize this and request initialization from it's manager too.
    /// </summary>
    [Client]
    public void initialize(ScapeProfile scapeProfile) {
      // set the loaded scape locally
      Saturn.LoadedProfiles.LoadedScape = scapeProfile;

      // subscribe the board controller to updates for tiles
      Barbelo.EventSystem.subscribe(
        BoardController,
        Events.WorldScapeEventSystem.Channels.TileUpdates
      );

      // initalize the board controller
      BoardController.initialize(Barbelo.ActiveScapeManager.scape.mainBoard);

      // initialize the scape manager
      Barbelo.SetActiveScape(
        GameObject.FindGameObjectWithTag("Scape Manager").GetComponent<ScapeManager>(),
        scapeProfile
      );
    }

    #endregion
  }
}