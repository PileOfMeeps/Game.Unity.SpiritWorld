using UnityEngine;

namespace SpiritWorld.Game.Controllers {
  public class StartUpMenuController : MonoBehaviour {

    /// <summary>
    /// Click the button to open the scape select menu
    /// </summary>
    void openScapeSelectMenu() {
      Saturn.Demiurge.displayScapeSelectMenu();
      Saturn.Demiurge.closeStartUpMenu();
    }

    /// <summary>
    /// Click the button to open the settings menu
    /// </summary>
    void openSettingsMenu() {
      Saturn.Demiurge.displaySettingsMenu();
      Saturn.Demiurge.closeStartUpMenu();
    }

    /// <summary>
    /// Click the button to open the settings menu
    /// </summary>
    void openCharacterSelectMenu() {
      Saturn.Demiurge.displayCharacterSelectMenu();
      Saturn.Demiurge.closeStartUpMenu();
    }

    /// <summary>
    /// Close the game
    /// </summary>
    void closeGame() {
      Application.Quit();
    }
  }
}
