﻿using UnityEditor;
using UnityEngine;

namespace SpiritWorld.Game.Controllers {
  public class TabbedMenuController : MonoBehaviour {

    #region Constants

    /// <summary>
    /// the prefab used to make the menu tabs
    /// </summary>
    public MenuTabController TabPrefab;

    /// <summary>
    /// A map of models used for tile features organized like so
    /// SET IN UNITY
    /// </summary>
    [SerializeField]
    public TabDataMap[] TabData;

    /// <summary>
    /// The index of the default tab
    /// </summary>
    public int DefaultTabIndex
      = 0;

    #endregion

    /// <summary>
    /// The tabs
    /// </summary>
    MenuTabController[] tabs;

    /// <summary>
    /// Populate the tabs
    /// </summary>
    void Awake() {
      int tabIndex = 0;
      tabs = new MenuTabController[TabData.Length];
      Transform tabArea = transform.GetChild(0).GetChild(0);
      foreach(TabDataMap tabData in TabData) {
        tabs[tabIndex] = MenuTabController.Make(
          TabPrefab,
          tabArea,
          this,
          tabIndex,
          tabData.Contents,
          tabData.Name,
          tabIndex == DefaultTabIndex,
          tabData.Icon
        );
      }
    }

    /// <summary>
    /// callback to tell all tabs except one to disable when another is clicked.
    /// </summary>
    /// <param name="id"></param>
    public void disableAllTabsExcept(int id) {
      foreach(MenuTabController tab in tabs) {
        if (tab.id != id) {
          tab.disable();
        }
      }
    }

    /// <summary>
    /// Set the content into the correct place in the hireracthy
    /// </summary>
    /// <param name="childContent"></param>
    public void addContentChild(Transform childContent) {
      childContent.SetParent(transform.GetChild(0).GetChild(1), false);
    }

    /// <summary>
    /// A map representing the models for one type of terrain feature sorted by how many usages it has left
    /// </summary>
    [System.Serializable]
    public class TabDataMap {

      /// <summary>
      /// The sprite to use for this type of tool
      /// </summary>
      [SerializeField]
      public Sprite Icon;

      /// <summary>
      /// the id of the tile feature this is for
      /// </summary>
      [SerializeField]
      public string Name;

      /// <summary>
      /// the gameobject containing the contents of this tab
      /// </summary>
      [SerializeField]
      public GameObject Contents;
    }
  }

  #region Unity Editor
#if UNITY_EDITOR

  /// <summary>
  /// Custom array drawer
  /// </summary>
  [CustomEditor(typeof(TabbedMenuController))]
  public class TabbedMenuControllerDrawer : Editor {
    public override void OnInspectorGUI() {
      serializedObject.Update();
      EditorList.Show(
        serializedObject.FindProperty("TabData"),
        EditorListOption.ButtonsBelow | EditorListOption.ListLabel | EditorListOption.Buttons | EditorListOption.ElementLabels,
        (element, id) => {
          return "Tab " + id + element.FindPropertyRelative("Name").stringValue;
        }
      );
      serializedObject.ApplyModifiedProperties();
    }
  }

#endif
  #endregion
}