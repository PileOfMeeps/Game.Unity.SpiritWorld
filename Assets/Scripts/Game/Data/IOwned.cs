﻿namespace SpiritWorld.Game.Data {

  /// <summary>
  /// A profile owned by another unique profile
  /// </summary>
  public interface IOwned {

    /// <summary>
    /// The id of the User profile that owns this.
    /// </summary>
    string owner {
      get;
    }
  }
}