﻿namespace SpiritWorld.Game.Data {

  /// <summary>
  /// indicates an object with a UUID
  /// </summary>
  public interface IUnique {

    /// <summary>
    /// the UUID of the object
    /// </summary>
    string id {
      get;
    }
  }
}
