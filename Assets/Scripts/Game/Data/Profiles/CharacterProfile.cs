﻿using SpiritWorld.World.Entities.Creatures;

namespace SpiritWorld.Game.Data {

  /// <summary>
  /// Data profile representing a user-created character
  /// </summary>
  [SQLite.Table("Character")]
  public class CharacterProfile : Profile, IOwned {

    #region SQLite Table Fields

    /// <summary>
    /// The id of the User profile that own's this.
    /// </summary>
    public string owner {
      get;
    }

    /// <summary>
    /// The version of the game used to generate this character
    /// </summary>
    public string version {
      get;
    } = Universe.GameVersion;

    #endregion

    /// <summary>
    /// The character profile's player character
    /// </summary>
    [SQLite.Ignore]
    public Player player {
      get;
    }

    /// <summary>
    /// Can only make this in private
    /// </summary>
    /// <param name="id"></param>
    /// <param name="name"></param>
    public CharacterProfile() : base() { }

    /// <summary>
    /// Make a new character profile
    /// </summary>
    /// <param name="id"></param>
    /// <param name="name"></param>
    public CharacterProfile(string name) : base(name) {}
  }
}
