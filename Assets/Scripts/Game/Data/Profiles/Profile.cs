﻿using Mirror;

namespace SpiritWorld.Game.Data {

  /// <summary>
  /// A profile used to load data from file, ex: a user, character, or scape profile
  /// </summary>
  public abstract class Profile : IUnique {

    #region SQLite Table Fields

    /// <summary>
    /// The id of the profile, should be UUID
    /// </summary>
    [SQLite.PrimaryKey]
    public string id {
      get => _id;
      set => _id = value;
    } [SyncVar] string _id;

    /// <summary>
    /// The user set name of the profile
    /// </summary>
    public string name {
      get => _name;
      set => _name = value;
    } [SyncVar] string _name;

    #endregion

    /// <summary>
    /// Base for making a profile
    /// </summary>
    protected Profile(string name) {
      id = Barbelo.UUID;
      this.name = name;
    }

    /// <summary>
    /// Base for making a profile
    /// </summary>
    protected Profile(string name, string id) {
      this.id = id;
      this.name = name;
    }

    /// <summary>
    /// Base for making a profile
    /// </summary>
    public Profile() {
      id = Barbelo.UUID;
    }
  }
}