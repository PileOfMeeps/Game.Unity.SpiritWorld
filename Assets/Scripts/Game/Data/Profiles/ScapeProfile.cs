﻿using SpiritWorld.World;
using System.Collections.Generic;

namespace SpiritWorld.Game.Data {

  /// <summary>
  /// Represents a saved scape
  /// </summary>
  [SQLite.Table("Scape")]
  public class ScapeProfile : Profile {

    #region Constants

    /// <summary>
    /// Save folder
    /// </summary>
    public const string SaveFolder
      = "/scapes/";

    #endregion

    #region SQLite Table Fields

    /// <summary>
    /// The id of the User profile that own's this.
    /// </summary>
    public string owner {
      get;
    }

    /// <summary>
    /// The type of worldscape this is
    /// </summary>
    public Scape.Type type {
      get;
    }

    /// <summary>
    /// How many seconds this world has been open for
    /// </summary>
    public float secondsplayed {
      get;
    } = 0;

    /// <summary>
    /// The size of the scape in discovered acers
    /// </summary>
    public int size {
      get;
    } = 0;

    /// <summary>
    /// The seed used to generate this scape
    /// </summary>
    public int seed {
      get;
    }

    /// <summary>
    /// The seed used to generate this scape
    /// </summary>
    public string intent {
      get;
    }

    /// <summary>
    /// The version of the game used to generate this scape
    /// </summary>
    public string version {
      get;
    } = Universe.GameVersion;

    /// <summary>
    /// If this scape is inaccessable unless logged in
    /// </summary>
    public bool isLocked {
      get;
    }

    #endregion

    /// <summary>
    /// The combined intent and seed values+
    /// </summary>
    [SQLite.Ignore]
    public int fullseed {
      get;
    }

    /// <summary>
    /// Where we save chunks to
    /// </summary>
    [SQLite.Ignore]
    string chunkSaveFolder
      => $"{SaveFolder}{name}.{id}/";

    /// <summary>
    /// Can only make this in private
    /// </summary>
    /// <param name="id"></param>
    /// <param name="name"></param>
    public ScapeProfile() : base() { }

    /// <summary>
    /// Make a new scape profile
    /// </summary>
    public ScapeProfile(string name, string intent, int seed, Scape.Type type) : base(name) {
      this.intent = intent;
      this.seed = seed;
      this.type = type;
      fullseed = int.Parse(Gematra(intent).ToString() + seed.ToString());
    }

    #region Gematra

    static Dictionary<char, int> GematraMap
      => new Dictionary<char, int>() {
        {'a', 1 },
        {'b', 2 },
        {'c', 3 },
        {'d', 4 },
        {'e', 5 },
        {'f', 6 },
        {'g', 7 },
        {'h', 8 },
        {'i', 9 },
        {'j', 10 },
        {'k', 20 },
        {'l', 30 },
        {'m', 40 },
        {'n', 50 },
        {'o', 60 },
        {'p', 70 },
        {'q', 80 },
        {'r', 90 },
        {'s', 100 },
        {'t', 200 },
        {'u', 300 },
        {'v', 400 },
        {'w', 500 },
        {'x', 600 },
        {'y', 700 },
        {'z', 800 }
      };

    static int Gematra(string intent) {
      int total = 0;
      foreach(char letter in intent) {
        if (int.TryParse(letter.ToString(), out int number)) {
          total += number;
        } else if (GematraMap.TryGetValue(letter, out int convertedValue)) {
          total += convertedValue;
        }
      }

      return total;
    }

    #endregion
  }
}