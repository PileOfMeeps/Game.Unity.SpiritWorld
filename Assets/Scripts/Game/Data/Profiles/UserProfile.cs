﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace SpiritWorld.Game.Data {

  /// <summary>
  /// A profile containing saved characters and scapes, along with a person's home-scape.
  /// </summary>
  [SQLite.Table("User")]
  public class UserProfile : Profile {

    /// <summary>
    /// The local profile based on the data stored on this device
    /// </summary>
    [SQLite.Ignore]
    public UserProfile localProfile
        => _localProfile ?? (_localProfile = DAO.GetLocalUserProfile());
    static UserProfile _localProfile;

    /// <summary>
    /// shortcut to the home scape profile.
    /// </summary>
    [SQLite.Ignore]
    public ScapeProfile defaultHomeScape
      => id != "" && homeScapes.Count > 0 ? homeScapes[0] : null;

    /// <summary>
    /// The character profiles associated with this user
    /// </summary>
    List<CharacterProfile> characters
      = new List<CharacterProfile>();

    /// <summary>
    /// The scapes associated with this user
    /// </summary>
    List<ScapeProfile> scapes
      = new List<ScapeProfile>();

    /// <summary>
    /// The scapes associated with this user
    /// </summary>
    List<ScapeProfile> homeScapes
      = new List<ScapeProfile>();

    /// <summary>
    /// Can only make this in private
    /// </summary>
    /// <param name="id"></param>
    /// <param name="name"></param>
    UserProfile(string name) : base(name) { }

    /// <summary>
    /// Can only make this in private
    /// </summary>
    /// <param name="id"></param>
    /// <param name="name"></param>
    UserProfile(string name, string id) : base(name, id) { }

    /// <summary>
    /// Can only make this in private
    /// </summary>
    /// <param name="id"></param>
    /// <param name="name"></param>
    public UserProfile() : base() { }

    /// <summary>
    /// Add the home scape for this player at position 0 of their scape profile array.
    /// </summary>
    /// <returns></returns>
    public ScapeProfile addHomeScape() {
      homeScapes.Insert(0, new ScapeProfile(
        name + "'s Own Little World", "Home",
        UnityEngine.Random.Range(0, 999999),
        World.Scape.Type.Home)
      );
      return scapes[0];
    }

    /// <summary>
    /// Get the test user profile
    /// </summary>
    public static UserProfile GetDummyProfile(string name) {
      return new UserProfile(name);
    }

    /// <summary>
    /// Data access object for User Profile data
    /// </summary>
    class DAO : SQLiteDAO<UserProfile> {

      /// <summary>
      /// Get the local user profile from the local sqlite DB
      /// </summary>
      /// <returns></returns>
      public static UserProfile GetLocalUserProfile() {
        UserProfile localProfile = new UserProfile("Local", "");
        LoadUserSubData(localProfile, new DAO());

        return localProfile;
      }

      /// <summary>
      /// Get the local user profile from the local sqlite DB
      /// </summary>
      /// <returns></returns>
      public static bool TryToLogInToUser(string username, string password, out UserProfile user) {
        DAO dao = new DAO();
        // TODO: LoginId should be stored in the foreign mysql db only
        // first get the user's salt value
        LoginId loginId = dao.connection.Query<LoginId>("SELECT userName, userId, salt FROM UserId WHERE userName = ?", username).FirstOrDefault();
        // use it with the pepper to validate the login
        if (loginId != null) {
          // hash the password with the salt we got and check if the value matches the DB value
          loginId = dao.connection.Query<LoginId>(
            "SELECT userName, userId FROM UserId WHERE userName = ? AND passHash = ?",
            username,
            HashPassword(password, loginId.salt)
          ).FirstOrDefault();
        }

        /// if we logged in, load the user profile
        if (loginId != null) {
          user = dao.getById(loginId.userId);
          LoadUserSubData(user, dao);

          return true;
        }

        user = null;
        return false;
      }

      /// <summary>
      /// Load the sub data profiles for the user profile
      /// </summary>
      static void LoadUserSubData(UserProfile user, DAO dao) {
        user.characters = SQLiteDAO<CharacterProfile>.GetByOwner(user.id, dao).ToList();
        user.scapes = SQLiteDAO<ScapeProfile>.GetWhere(scape => scape.owner == user.id || !scape.isLocked, dao).ToList();
        user.homeScapes = new List<ScapeProfile>();
        user.scapes.RemoveAll(scape => {
          if (scape.type == World.Scape.Type.Home) {
            user.homeScapes.Add(scape);

            return true;
          }

          return false;
        });
      }

      /// <summary>
      /// Hash a password
      /// </summary>
      /// <param name="password"></param>
      /// <param name="salt"></param>
      /// <returns></returns>
      static string HashPassword(string password, string salt) {
        string pepper = "SATOR1AREPO4TENET5OPERA6ROTAS12";
        Rfc2898DeriveBytes rfcKey = new Rfc2898DeriveBytes(
          password + pepper,
          Encoding.ASCII.GetBytes(salt),
          1236
        );
        return EncryptString(
          Convert.ToBase64String(rfcKey.GetBytes(12)),
          Convert.ToBase64String(rfcKey.GetBytes(32))
        );
      }

      /// <summary>
      /// encrypt a string with a key
      /// </summary>
      /// <param name="key"></param>
      /// <param name="plainText"></param>
      /// <returns></returns>
      public static string EncryptString(string key, string plainText) {
        byte[] iv = new byte[128];
        byte[] array;

        using (Aes aes = Aes.Create()) {
          aes.Key = Encoding.UTF8.GetBytes(key);
          aes.IV = iv;

          ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

          using MemoryStream memoryStream = new MemoryStream();
          using CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
          using (StreamWriter streamWriter = new StreamWriter(cryptoStream)) {
            streamWriter.Write(plainText);
          }

          array = memoryStream.ToArray();
        }

        return Convert.ToBase64String(array);
      }

      /// <summary>
      /// decrypt a string weith a key
      /// </summary>
      /// <param name="key"></param>
      /// <param name="cipherText"></param>
      /// <returns></returns>
      public static string DecryptString(string key, string cipherText) {
        byte[] iv = new byte[16];
        byte[] buffer = Convert.FromBase64String(cipherText);

        using Aes aes = Aes.Create();
        aes.Key = Encoding.UTF8.GetBytes(key);
        aes.IV = iv;
        ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

        using MemoryStream memoryStream = new MemoryStream(buffer);
        using CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
        using StreamReader streamReader = new StreamReader(cryptoStream);
        return streamReader.ReadToEnd();
      }

      /// <summary>
      /// Class for the login data table
      /// </summary>
      [SQLite.Table("UserId")]
      class LoginId : IUnique {

        [SQLite.Ignore]
        public string id
          => "";

        /// <summary>
        /// name of the user
        /// </summary>
        public string userName {
          get;
        }

        /// <summary>
        /// id of the user
        /// </summary>
        public string userId {
          get;
        }

        /// <summary>
        /// Password hashed with salt and pepper
        /// </summary>
        public string passhash {
          get;
        }

        /// <summary>
        /// salt for the password hash
        /// </summary>
        public string salt {
          get;
        }

        public LoginId() { }
      }
    }
  }
}
