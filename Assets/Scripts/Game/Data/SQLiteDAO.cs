﻿using SQLite;
using System;
using System.Collections.Generic;

namespace SpiritWorld.Game.Data {

  /// <summary>
  /// The base dao for fetching data from SQLITE
  /// </summary>
  /// <typeparam name="FetchType"></typeparam>
  public abstract class SQLiteDAO<FetchType> : SQLiteDAOBase where FetchType : IUnique, new() {

    /// <summary>
    /// The tables we know exist already
    /// </summary>
    static HashSet<Type> ExistingTables
      => new HashSet<Type>();

    /// <summary>
    /// Make a dao w/ an existing connection
    /// </summary>
    public SQLiteDAO(SQLiteConnection connection = null) : base(connection) {
      VerifyTableExists(connection);
    }

    /// <summary>
    /// Get the object of this type from the DB loaded by it's id.
    /// </summary>
    public FetchType getById(string id) {
      return connection.Table<FetchType>().Where(row => row.id == id).FirstOrDefault();
    }

    /// <summary>
    /// Get all of the given type by it's owner's id.
    /// </summary>
    public static FetchType[] GetByOwner(string ownerId, SQLiteDAOBase dao = null) {
      SQLiteConnection connection = dao.connection ?? Connect();
      VerifyTableExists(connection);
      if (typeof(FetchType) is IOwned) {
        return connection.Table<FetchType>().Where(r => (r as IOwned).owner == ownerId).ToArray();
      }

      return default;
    }

    /// <summary>
    /// Get all of the given type by it's owner's id.
    /// </summary>
    public static FetchType[] GetByOwner(string ownerId, Func<FetchType, bool> where, SQLiteDAOBase dao = null) {
      SQLiteConnection connection = dao.connection ?? Connect();
      VerifyTableExists(connection);
      if (typeof(FetchType) is IOwned) {
        return connection.Table<FetchType>().Where(row => (row as IOwned).owner == ownerId && where(row)).ToArray();
      }

      return default;
    }

    /// <summary>
    /// Get all of the given type by it's owner's id.
    /// </summary>
    public static FetchType[] GetWhere(Func<FetchType, bool> where, SQLiteDAOBase dao = null) {
      SQLiteConnection connection = dao.connection ?? Connect();
      VerifyTableExists(connection);
      return connection.Table<FetchType>().Where(row => where(row)).ToArray();
    }

    /// <summary>
    /// Get all of the given type by it's owner's id.
    /// </summary>
    public static FetchType GetFirstWhere(Func<FetchType, bool> where, SQLiteDAOBase dao = null) {
      SQLiteConnection connection = dao.connection ?? Connect();
      VerifyTableExists(connection);
      return connection.Table<FetchType>().Where(row => where(row)).FirstOrDefault();
    }

    /// <summary>
    /// Check if the table exists, and if not create it
    /// </summary>
    static void VerifyTableExists(SQLiteConnection connection) {
      Type tableType = typeof(FetchType);
      if (!ExistingTables.Contains(tableType)) {
        TableAttribute tableAttribute = (TableAttribute)Attribute.GetCustomAttribute(tableType, typeof(TableAttribute));
        SQLiteCommand query = connection.CreateCommand(
          @"Exists (
            SELECT * FROM sqlite_master WHERE type = 'table' AND tbl_name =?;
          )",
          tableAttribute?.Name ?? tableType.Name
        );
        if (query.ExecuteScalar<string>() != null) {
          ExistingTables.Add(tableType);
        } else {
          connection.CreateTable<FetchType>();
        }
      }
    }
  }
}
