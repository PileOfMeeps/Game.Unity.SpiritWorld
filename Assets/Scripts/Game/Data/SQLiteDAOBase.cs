﻿using SQLite;
using UnityEngine;

namespace SpiritWorld.Game.Data {
  public class SQLiteDAOBase {

    /// <summary>
    /// The connection for this DAO
    /// </summary>
    public SQLiteConnection connection {
      get;
    }

    /// <summary>
    /// Make a dao w/ an existing connection
    /// </summary>
    protected SQLiteDAOBase(SQLiteConnection connection = null) {
      this.connection = connection ?? Connect();
    }

    /// <summary>
    /// Get a new DB connection
    /// </summary>
    /// <returns></returns>
    protected static SQLiteConnection Connect() {
      return new SQLiteConnection(Application.persistentDataPath + "/DB/EvixCore.db");
    }
  }
}