﻿using Mirror;
using SpiritWorld.Game.Data;
using SpiritWorld.Game.Controllers;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using SpiritWorld.Events;

namespace SpiritWorld.Game {

  /// <summary>
  /// The Demiurge of the Universe
  /// </summary>
  public class Demiurge : MonoBehaviour {

    #region Constants

    ///SET IN UNITY
    //////////////////////

    /// <summary>
    /// The event system object used by Barbelo server to talk to everything
    /// </summary>
    public WorldScapeEventSystem EventSystem;

    /// <summary>
    /// The void used for the menu on it's own
    /// </summary>
    public SceneReference MenuVoid;

    /// <summary>
    /// The scene for the selected player homeworld
    /// </summary>
    public SceneReference HomeWorld;

    /// <summary>
    /// The scene for the endless scape of chunks
    /// </summary>
    public SceneReference EndlessScape;

    /// <summary>
    /// The start up menu
    /// </summary>
    public SceneReference StartupMenuUI;

    /// <summary>
    /// The scape select menu
    /// </summary>
    public SceneReference ScapeSelectMenuUI;

    /// <summary>
    /// The UI displayed in the top right of some menus showing the current selected character profile
    /// </summary>
    public SceneReference SelectedCharacterInfoIconUI;

    /// <summary>
    /// The scape select menu
    /// </summary>
    public SceneReference CharacterSelectMenuUI;

    /// <summary>
    /// The scape select menu
    /// </summary>
    public SceneReference SettingsMenuUI;

    /// <summary>
    /// The local player interface for an overworld
    /// </summary>
    public SceneReference LocalPlayerOverworldUI;

    #endregion

    #region Initialization

    /// <summary>
    /// Begin
    /// </summary>
    void Awake() {
      /// Declare saturn the Demiurge (Static Local controller) and set up the event system so Barbelo (Managers) can talk to Saturn (Controllers)
      Saturn.Demiurge = this;
      Barbelo.EventSystem = EventSystem;
    }

    /// <summary>
    /// On start
    /// </summary>
    void Start() {
      /// try to log in if we have a saved main user profile
      Saturn.LoadedProfiles.TryToLogIn("Meep", "");

      /// load out background
      ScapeProfile homeScape = Saturn.LoadedProfiles.CurrentUser.defaultHomeScape
        ?? Saturn.LoadedProfiles.CurrentUser.localProfile.defaultHomeScape
        ?? Saturn.LoadedProfiles.CurrentUser.localProfile.addHomeScape();
      if (homeScape != null) {
        // load the home scape background scene
        displayHomeScape(homeScape);
      } else {
        // load the empty void background scene
        displayMenuVoid();
      }

      /// load the welcome menu
      displayStartupMenu();
    }

    #endregion

    #region Scene Control

    ///// MENUS
    //////////////////////////////////////////

    /// <summary>
    /// load then display the welcome screen scene
    /// </summary>
    [Client]
    void displayStartupMenu() {
      StartCoroutine(loadScene(StartupMenuUI, false));
    }

    /// <summary>
    /// unload and hide  welcom screen scene
    /// </summary>
    [Client]
    public void closeStartUpMenu() {
      SceneManager.UnloadSceneAsync(StartupMenuUI);
    }

    /// <summary>
    /// load then display the scape select menu.
    /// </summary>
    [Client]
    public void displayScapeSelectMenu() {
      StartCoroutine(loadScene(ScapeSelectMenuUI, false));
    }

    /// <summary>
    /// unload and hide the scape select menu
    /// </summary>
    [Client]
    void closeScapeSelectMenu() {
      SceneManager.UnloadSceneAsync(ScapeSelectMenuUI);
    }

    /// <summary>
    /// load then display the settings menu
    /// </summary>
    [Client]
    public void displaySettingsMenu() {
      StartCoroutine(loadScene(SettingsMenuUI, false));
    }

    /// <summary>
    /// load then display the character select menu
    /// </summary>
    [Client]
    public void displayCharacterSelectMenu() {
      StartCoroutine(loadScene(CharacterSelectMenuUI, false));
    }

    /// <summary>
    /// Turn on the local player's overworld controller and UI and hook it up to the universe
    /// </summary>
    [Client]
    void displayLocalPlayerOverworldUI() {
      StartCoroutine(loadScene(LocalPlayerOverworldUI, () => {
        Saturn.LocalPlayerController = GameObject.FindGameObjectWithTag("Local Player Controller").GetComponent<PlayerController>();
        Saturn.LocalPlayerController.initialize(Saturn.LoadedProfiles.SelectedCharacter.player);
      }));
    }

    [Client]
    void closeLocalPlayerOverworldUI() {
      SceneManager.UnloadSceneAsync(LocalPlayerOverworldUI);
      Saturn.LocalPlayerController = null;
    }

    ///// LOCATIONS
    //////////////////////////////////////////

    /// <summary>
    /// The void used in case no home world loads as a menu bg
    /// </summary>
    [Client]
    void displayMenuVoid() {
      SceneManager.LoadSceneAsync(MenuVoid, LoadSceneMode.Additive);
    }

    /// <summary>
    /// Turn on the given home scape and hook it up to the universe
    /// </summary>
    [Client]
    void displayHomeScape(ScapeProfile homeScapeProfile) {
      StartCoroutine(loadScene(HomeWorld, () => {
        Saturn.ScapeController = GameObject.FindGameObjectWithTag("Scape Controller").GetComponent<ScapeController>();
        Saturn.ScapeController.initialize(homeScapeProfile);
      }));
    }

    /// <summary>
    /// Load in the endless scape world and then initialize it
    /// </summary>
    [Client]
    /// <param name="endlessScapeProfile"></param>
    void displayEndlessScapeScene(ScapeProfile endlessScapeProfile) {
      StartCoroutine(loadScene(EndlessScape, () => {
        Saturn.ScapeController = GameObject.FindGameObjectWithTag("Scape Controller").GetComponent<ScapeController>();
        Saturn.ScapeController.initialize(endlessScapeProfile);
      }));
    }

    ///// Utility
    /////////////////////////////////////////

    /// <summary>
    /// used to load a scene
    /// </summary>
    /// <param name="scene"></param>
    /// <param name="onLoadingComplete"></param>
    /// <param name="hideSceneUntilLoadingComplete"></param>
    /// <param name="whileLoading"></param>
    /// <returns></returns>
    [Client]
    IEnumerator loadScene(SceneReference scene, Action onLoadingComplete, bool hideSceneUntilLoadingComplete = false, Action<AsyncOperation> whileLoading = null) {
      AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
      asyncOperation.allowSceneActivation = !hideSceneUntilLoadingComplete;

      while (!asyncOperation.isDone) {
        whileLoading?.Invoke(asyncOperation);
        yield return null;
      }
      asyncOperation.allowSceneActivation = true;
      onLoadingComplete();
    }

    /// <summary>
    /// Shortcut for loading a scene hidden with no callback
    /// </summary>
    /// <param name="scene"></param>
    /// <param name="hideSceneUntilLoadingComplete"></param>
    /// <returns></returns>
    [Client]
    IEnumerator loadScene(SceneReference scene, bool hideSceneUntilLoadingComplete) {
      return loadScene(scene, () => { }, hideSceneUntilLoadingComplete);
    }

    #endregion
  }
}
