﻿using Mirror;
using SpiritWorld.Events;
using SpiritWorld.Game.Controllers;
using SpiritWorld.Inventories;
using SpiritWorld.Inventories.Items;
using SpiritWorld.World.Entities.Creatures;
using SpiritWorld.World.Terrain.TileGrid;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SpiritWorld.Managers {

  /// <summary>
  /// Manages a creature or object's local inventories
  /// </summary>
  public abstract class InventoryManager : NetworkBehaviour {

    /// <summary>
    /// A link to the parent's worldspace
    /// </summary>
    Transform worldSpaceParent;

    /// <summary>
    /// Return the inventory we want given the enum type
    /// </summary>
    /// <param name="inventoryType"></param>
    /// <returns></returns>
    protected abstract IInventory getInventoryFromType(Enum inventoryType = null);

    /// <summary>
    /// get the inventories to put matching loot into in order
    /// </summary>
    /// <returns></returns>
    protected abstract (IInventory inventory, Enum inventoryType)[] getInventoriesInMatchPriorityOrder();

    /// <summary>
    /// Get the inventories to put new loot into in order
    /// </summary>
    /// <returns></returns>
    protected abstract (IInventory inventory, Enum inventoryType)[] getInventoriesInLootPriorityOrder();

    /// <summary>
    /// initialize an inventory manager
    /// </summary>
    /// <param name="worldSpaceParent">The transform of the object or creature who's inventory this is managing. used for dropping items</param>
    [Server]
    public void initialize(Transform worldSpaceParent) {
      this.worldSpaceParent = worldSpaceParent;
    }

    /// <summary>
    /// Try to pick up the given item stack
    /// </summary>
    [Command]
    public virtual void tryToPickUp(Item itemStack) {
      tryToAddItemToAnyInventory(itemStack, out _, out _);
    }

    /// <summary>
    /// Try to pick up the given item drop in the world
    /// </summary>
    [Command]
    public virtual void tryToPickUpDrop(ActiveDropData itemDropChunkData) {
      Item itemDrop = Barbelo.ActiveScapeManager.activeBoard[itemDropChunkData.chunkAxialKey].getDrop(itemDropChunkData.dropId);
      tryToAddItemToAnyInventory(itemDrop, out Item addedItem, out _);
      if (addedItem.Equals(itemDrop)) {
        // remove the drop from the server and delete it from the clients too
        Barbelo.ActiveScapeManager.activeBoard[itemDropChunkData.chunkAxialKey].remove(itemDropChunkData.dropId);
        Barbelo.EventSystem.notifyChannelOf(
          new ItemDropController.PickedUpEvent(itemDropChunkData),
          WorldScapeEventSystem.Channels.TileUpdates
        );
      }
    }

    /// <summary>
    /// Try to take all the items from the given inventory
    /// </summary>
    [Command]
    public virtual void tryToLoot(Inventory inventory) {
      tryToLoot(inventory, out _, out _);
    }

    /// <summary>
    /// Try to add an item to the inventory at the given slot
    /// </summary>
    [Command]
    public virtual void tryToAddToInventoryAt(
      Item item,
      PlayerInventorySlot newLocation,
      PlayerInventorySlot oldLocation
    ) {
      tryToAddItemAt(item, newLocation, oldLocation, out _);
    }

    /// <summary>
    /// Try to swap the item intenvtory slots out
    /// </summary>
    /// <param name="movedItemOriginalSlot">The original slot of the the item taking the old items place</param>
    /// <param name="slotDroppedInto">the new slot we're dropping the new item in, the one with the old item in it</param>
    [Command]
    public virtual void tryToSwapInventoryItems(
      PlayerInventorySlot movedItemOriginalSlot,
      PlayerInventorySlot slotDroppedInto
    ) {
      Item movedItem = null;
      // make sure it's not the same spot.
      if (!(movedItemOriginalSlot.containingInventory == slotDroppedInto.containingInventory && slotDroppedInto.pivot.Equals(movedItemOriginalSlot.pivot))) {
        // get and add the item we want to the new inventory slot from it's old one on the backend.
        if (movedItemOriginalSlot.containingInventory != Player.InventoryTypes.Shortcuts && movedItemOriginalSlot.containingInventory != Player.InventoryTypes.None) {
          removeItemAt(movedItemOriginalSlot.containingInventory, movedItemOriginalSlot.pivot, out movedItem);
        }

        // if we got an item to move
        if (movedItem != null) {
          // if we're putting it into no inventory, just toss it out
          if (slotDroppedInto.containingInventory == Player.InventoryTypes.None) {
            dropItem(movedItem);
          }

          // if we have an item to add to an inventory, try to do it
          tryToAddToInventoryAt(
            movedItem,
            slotDroppedInto,
            movedItemOriginalSlot
          );

          // TODO: will this destory the item source for items who aren't dropped correctly into an inventory?
          if (movedItemOriginalSlot.containingInventory == Player.InventoryTypes.None) {
            TargetSelf_destroyItemDragIcon();
            TargetAll_destroyItemPickupSource();
          } else {
            TargetSelf_resetItemDragger();
          }
        }
      }
    }

    /// <summary>
    /// Try to remove an item from a grid based inventory at the given location
    /// </summary>
    [Command]
    public virtual void removeFromInventoryAt(Player.InventoryTypes inventoryType, Coordinate pivotLocation) {
      removeItemAt(inventoryType, pivotLocation, out _);
    }

    /// <summary>
    /// Logic for internal notifications for tryToAddToInventoryAt
    /// </summary>
    [Server]
    protected virtual void tryToAddItemAt(
      Item item,
      PlayerInventorySlot placedItemSlot,
      PlayerInventorySlot originalItemSlot,
      out Item succesfullyAddedItem
    ) {
      succesfullyAddedItem = null;
      Item leftovers = getInventoryFromType(placedItemSlot.containingInventory) is GridBasedInventory gridInventory
        ? gridInventory.tryToAdd(item, placedItemSlot.pivot, out succesfullyAddedItem)
        : null;

      // if we have leftovers, put them back, reset things, and update the inventory
      if (leftovers != null && !(leftovers.type is IShortcut)) {
        // drop the leftovers if we replaced them with a shortcut
        if (originalItemSlot.containingInventory == Player.InventoryTypes.Shortcuts) {
          dropItem(leftovers);
          // otherwise try to slot them back in where we came from unless it came from nowhere.
          // TODO: test this
        } else if (originalItemSlot.containingInventory != Player.InventoryTypes.None) {
          Barbelo.LocalPlayerManager.InventoryManager.tryToAddToInventoryAt(
            leftovers,
            originalItemSlot,
            originalItemSlot
          );
        }
      }
    }

    /// <summary>
    /// Shortcut that has an item return for internal use
    /// </summary>
    [Server]
    protected void removeItemAt(Player.InventoryTypes inventoryType, Coordinate pivotLocation, out Item removedItem) {
      removedItem = getInventoryFromType(inventoryType) is GridBasedInventory gridInventory
        ? gridInventory.removeAt(pivotLocation)
        : null;

      /// alert the world of a drop event.
      if (removedItem != null && !(removedItem.type is IShortcut)) {
        dropItem(removedItem);
      }
    }

    /// <summary>
    /// Try to pick up an item stack
    /// </summary>
    [Server]
    protected Item tryToAddItemToAnyInventory(Item itemStack, out Item succesfullyAddedStack, out (Coordinate, Enum)[] modifiedPivots) {
      succesfullyAddedStack = null;
      List<(Coordinate, Enum)> updatedPivots = new List<(Coordinate, Enum)>();
      Item leftovers = itemStack;

      /// check the matching inventory order for any item matches first
      // first put all the matches in the hot bar
      foreach ((IInventory matchInventory, Enum inventoryType) in getInventoriesInMatchPriorityOrder()) {
        // if we find a matching item and this is a grid inventory
        if (matchInventory.search(existingItem => existingItem != null
          && existingItem.canStackWith(leftovers)).Length > 0
        ) {
          // if it's a grid inventory, record pivots
          if (matchInventory is GridBasedInventory gridInventory) {
            leftovers = gridInventory.tryToAdd(leftovers, out Item succesfullyAddedItem, out Coordinate[] changedPivots);
            if (succesfullyAddedItem != null) {
              if (succesfullyAddedStack == null) {
                succesfullyAddedStack = succesfullyAddedItem;
              } else {
                succesfullyAddedStack.addToStack(succesfullyAddedItem, out _);
              }
            }
            foreach (Coordinate pivot in changedPivots) {
              updatedPivots.Add((pivot, inventoryType));
            }
            // if it's not a grid inventory, just cram it in
          } else {
            leftovers = matchInventory.tryToAdd(itemStack, out Item succesfullyAddedItem);
            if (succesfullyAddedItem != null) {
              if (succesfullyAddedStack == null) {
                succesfullyAddedStack = succesfullyAddedItem;
              } else {
                succesfullyAddedStack.addToStack(succesfullyAddedItem, out _);
              }
            }
          }
        }
      }

      /// add them to the loot inventories if we still have new items to add
      if (leftovers != null) {
        foreach ((IInventory lootInventory, Enum inventoryType) in getInventoriesInLootPriorityOrder()) {
          if (lootInventory is GridBasedInventory gridInventory) {
            leftovers = gridInventory.tryToAdd(leftovers, out Item succesfullyAddedItem, out Coordinate[] changedPivots);
            foreach (Coordinate pivot in changedPivots) {
              updatedPivots.Add((pivot, inventoryType));
            }
            if (succesfullyAddedItem != null) {
              if (succesfullyAddedStack == null) {
                succesfullyAddedStack = succesfullyAddedItem;
              } else {
                succesfullyAddedStack.addToStack(succesfullyAddedItem, out _);
              }
            }
          } else {
            leftovers = lootInventory.tryToAdd(leftovers, out Item succesfullyAddedItem);
            if (succesfullyAddedItem != null) {
              if (succesfullyAddedStack == null) {
                succesfullyAddedStack = succesfullyAddedItem;
              } else {
                succesfullyAddedStack.addToStack(succesfullyAddedItem, out _);
              }
            }
          }
          if (leftovers == null || leftovers.quantity <= 0) {
            break;
          }
        }
      }

      modifiedPivots = updatedPivots.ToArray();
      return leftovers;
    }


    /// <summary>
    /// try to loot with pivot updates
    /// </summary>
    [Server]
    public virtual Item[] tryToLoot(IInventory inventory, out Item[] succesfullyAddedUpItems, out (Coordinate, Enum)[] modifiedPivots) {
      List<(Coordinate, Enum)> updatedPivots = new List<(Coordinate, Enum)>();
      BasicInventory leftovers = new BasicInventory();
      List<Item> sucessfullyAddedStacks = new List<Item>();

      /// check the matching inventory order for any item matches first
      (IInventory, Enum)[] matchInventories = getInventoriesInMatchPriorityOrder();
      foreach (Item item in inventory.empty()) {
        bool itemPlaced = false;
        // first put all the matches in the hot bar
        foreach ((IInventory matchInventory, Enum inventoryType) in matchInventories) {
          // if we find a matching item and this is a grid inventory
          if (matchInventory.search(existingItem => existingItem != null
            && existingItem.canStackWith(item)).Length > 0
          ) {
            // if it's a grid inventory, record pivots
            if (matchInventory is GridBasedInventory gridInventory) {
              Item leftoverStack = gridInventory.tryToAdd(item, out Item succesfullyAddedStack, out Coordinate[] changedPivots);
              if (leftoverStack != null) {
                leftovers.tryToAdd(leftoverStack, out _);
              }
              if (succesfullyAddedStack != null) {
                sucessfullyAddedStacks.Add(succesfullyAddedStack);
              }
              foreach (Coordinate pivot in changedPivots) {
                updatedPivots.Add((pivot, inventoryType));
              }
              // if it's not a grid inventory, just cram it in
            } else {
              Item leftoverStack = matchInventory.tryToAdd(item, out Item succesfullyAddedStack);
              if (leftoverStack != null) {
                leftovers.tryToAdd(leftoverStack, out _);
              }
              if (succesfullyAddedStack != null) {
                sucessfullyAddedStacks.Add(succesfullyAddedStack);
              }
            }

            // we found it in this inventory, we can break
            itemPlaced = true;
            break;
          }
        }

        if (!itemPlaced) {
          leftovers.tryToAdd(item, out _);
        }
      }

      /// check the looting inventories for all new items
      (IInventory, Enum)[] lootInventories = getInventoriesInMatchPriorityOrder();
      Item[] remainingLeftovers = null;
      foreach ((IInventory lootInventory, Enum inventoryType) in lootInventories) {
        if (lootInventory is GridBasedInventory gridInventory) {
          remainingLeftovers = gridInventory.tryToLoot(leftovers, out Item[] succesfullyAddedItems, out Coordinate[] changedPivots);
          foreach (Coordinate pivot in changedPivots) {
            updatedPivots.Add((pivot, inventoryType));
          }
          sucessfullyAddedStacks.AddRange(succesfullyAddedItems);
        } else {
          remainingLeftovers = leftovers.emptyInto(lootInventory, out Item[] succesfullyAddedItems);
          sucessfullyAddedStacks.AddRange(succesfullyAddedItems);
        }
        if (remainingLeftovers.Length <= 0) {
          break;
        }
      }

      modifiedPivots = updatedPivots.ToArray();
      succesfullyAddedUpItems = sucessfullyAddedStacks.ToArray();
      return remainingLeftovers;
    }

    /// <summary>
    /// Tell the game to drop the given item into the world
    /// </summary>
    [Server]
    protected void dropItem(Item item) {
      Tile droppedOnTile = Barbelo.ActiveScapeManager.activeBoard.get(worldSpaceParent.position);
      Item[] items = new Item[] { item };
      /// alert the world of a drop event.
      Barbelo.EventSystem.notifyChannelOf(
        new DropItemsEvent(
          droppedOnTile.worldLocation,
          items,
          Barbelo.ActiveScapeManager.activeBoard[droppedOnTile.parentChunkKey].add(items)
        ),
        Events.WorldScapeEventSystem.Channels.TileUpdates
      );
    }

    #region RPC Client Callbacks

    /// <summary>
    /// Callback to destroy the item source and icon on the client
    /// TODO: test these callbacks
    /// </summary>
    [TargetRpc]
    void TargetSelf_destroyItemDragIcon() {
      ItemInventoryDragController.ItemBeingDragged.destroyIcon();
    }

    /// <summary>
    /// Callback to destroy the item source and icon on the client
    /// TODO: test these callbacks
    /// </summary>
    [ClientRpc]
    void TargetAll_destroyItemPickupSource() {
      ItemInventoryDragController.ItemBeingDragged.destroyPickupSource();
    }

    /// <summary>
    /// Callback to destroy the item source and icon on the client
    /// </summary>
    [TargetRpc]
    void TargetSelf_resetItemDragger() {
      ItemInventoryDragController.ItemBeingDragged.resetToOriginalPosition();
    }

    #endregion

    /// <summary>
    /// Struct for serializing a player item slot
    /// </summary>
    public struct PlayerInventorySlot {

      /// <summary>
      /// The inventory containing this item
      /// </summary>
      public Player.InventoryTypes containingInventory {
        get;
      }

      /// <summary>
      /// The pivot/location the item is placed in the inventory
      /// </summary>
      public Coordinate pivot {
        get;
      }

      /// <summary>
      /// Make a slot
      /// </summary>
      /// <param name="pivot"></param>
      /// <param name="containingInventory"></param>
      public PlayerInventorySlot(Player.InventoryTypes containingInventory, Coordinate pivot) {
        this.pivot = pivot;
        this.containingInventory = containingInventory;
      }

      /// <summary>
      /// get from a tuple
      /// </summary>
      /// <param name="itemSlotData"></param>
      public static implicit operator PlayerInventorySlot((Player.InventoryTypes inventory, Coordinate pivot) itemSlotData) {
        return new PlayerInventorySlot(itemSlotData.inventory, itemSlotData.pivot);
      }

      /// <summary>
      /// turn into a tuple
      /// </summary>
      /// <param name="itemSlotData"></param>
      public static implicit operator (Player.InventoryTypes containingInventory, Coordinate pivot)(PlayerInventorySlot inventorySlot) {
        return (inventorySlot.containingInventory, inventorySlot.pivot);
      }
    }

    /// <summary>
    /// Struct for serializing a player item slot
    /// </summary>
    public struct ActiveDropData {

      /// <summary>
      /// The inventory containing this item
      /// </summary>
      public int dropId {
        get;
      }

      /// <summary>
      /// The pivot/location the item is placed in the inventory
      /// </summary>
      public Coordinate chunkAxialKey {
        get;
      }

      /// <summary>
      /// Make a slot
      /// </summary>
      /// <param name="pivot"></param>
      /// <param name="containingInventory"></param>
      public ActiveDropData(int dropId, Coordinate chunkAxialKey) {
        this.dropId = dropId;
        this.chunkAxialKey = chunkAxialKey;
      }

      /// <summary>
      /// get from a tuple
      /// </summary>
      /// <param name="itemSlotData"></param>
      public static implicit operator ActiveDropData((int dropId, Coordinate chunkAxialKey) itemSlotData) {
        return new ActiveDropData(itemSlotData.dropId, itemSlotData.chunkAxialKey);
      }

      /// <summary>
      /// turn into a tuple
      /// </summary>
      /// <param name="itemSlotData"></param>
      public static implicit operator (int dropId, Coordinate chunkAxialKey)(ActiveDropData inventorySlot) {
        return (inventorySlot.dropId, inventorySlot.chunkAxialKey);
      }
    }

    #region Events

    /// <summary>
    /// event for player picking up an item
    /// </summary>
    public struct DropItemsEvent : IEvent {
      public string name
        => "Something dropped an item";

      /// <summary>
      /// The item that was picked up
      /// </summary>
      public Item[] items {
        get;
      }

      /// <summary>
      /// The player who picked it up
      /// </summary>
      public Vector3 playerWorldLocation {
        get;
      }

      /// <summary>
      /// The id of the drop in the chunk it's managed by
      /// </summary>
      public int[] dropIds {
        get;
      }

      /// <summary>
      /// Make an event of this kind
      /// </summary>
      /// <param name="player"></param>
      /// <param name="item"></param>
      public DropItemsEvent(Vector3 playerWorldLocation, Item[] items, int[] dropIds) {
        this.items = items;
        this.playerWorldLocation = playerWorldLocation;
        this.dropIds = dropIds;
      }
    }

    #endregion
  }
}
