﻿using Mirror;
using SpiritWorld.Events;
using SpiritWorld.Inventories;
using SpiritWorld.Inventories.Items;
using SpiritWorld.World.Entities.Creatures;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SpiritWorld.Managers {

  /// <summary>
  /// Inventory manager for a player character
  /// </summary>
  public class PlayerCharacterInventoryManager : InventoryManager {

    /// <summary>
    /// This's associated player manager
    /// </summary>
    PlayerManager playerManager;

    #region Initialization

    /// <summary>
    /// Initialize this with the parent manager.
    /// </summary>
    [Server]
    public void initialize(PlayerManager playerManager) {
      this.playerManager = playerManager;
    }

    #endregion

    #region Data Accessors

    /// <summary>
    /// Get the player inventory of the given type
    /// </summary>
    /// <param name="inventoryType"></param>
    /// <returns></returns>
    protected override IInventory getInventoryFromType(Enum inventoryType = null) {
      return inventoryType switch
      {
        Player.InventoryTypes playerType => playerType switch
        {
          Player.InventoryTypes.GridPack => playerManager.player.packInventory,
          Player.InventoryTypes.HotBar => playerManager.player.hotBarInventory,
          Player.InventoryTypes.Pockets => playerManager.player.pocketInventory,
          _ => throw new ArgumentException(playerType + " does not have a valid ILocalPlayerInventoryController"),
        },
        _ => throw new ArgumentException(inventoryType + " does not have a valid ILocalPlayerInventoryController"),
      };
    }

    ///// Constants used for looting
    ///////////////////////////////////////

    protected override (IInventory inventory, Enum inventoryType)[] getInventoriesInLootPriorityOrder() {
      return new (IInventory, Enum)[] {
        (playerManager.player.packInventory, Player.InventoryTypes.GridPack),
        (playerManager.player.hotBarInventory, Player.InventoryTypes.HotBar)
      };
    }

    protected override (IInventory inventory, Enum inventoryType)[] getInventoriesInMatchPriorityOrder() {
      return new (IInventory, Enum)[] {
        (playerManager.player.hotBarInventory, Player.InventoryTypes.HotBar),
        (playerManager.player.packInventory, Player.InventoryTypes.GridPack)
      };
    }

    #endregion

    #region Inventory Managment

    /// <summary>
    /// Try to pick up the given item stack
    /// </summary>
    /// <param name="itemStack"></param>
    /// <returns></returns>
    public override void tryToPickUp(Item itemStack) {
      Item leftovers = tryToAddItemToAnyInventory(itemStack, out Item succesfullyPickedUpItem, out (Coordinate pivot, Enum)[] modifiedPivots);

      // update local notifications
      if (succesfullyPickedUpItem != null) {
        Barbelo.EventSystem.notifyChannelOf(
          new ObtainItemsEvent(playerManager.player, succesfullyPickedUpItem),
          Events.WorldScapeEventSystem.Channels.LocalPlayerUpdates
        );
      }

      // update local grid inventory if needed
      if (modifiedPivots.Length > 0) {
        Barbelo.EventSystem.notifyChannelOf(
          new ItemsUpdatedEvent(modifiedPivots.Select(x => x.pivot).ToArray(), Player.InventoryTypes.GridPack),
          Events.WorldScapeEventSystem.Channels.LocalPlayerUpdates
        );
      }

      // drop the leftovers
      dropItem(leftovers);
    }

    /// <summary>
    /// Try to take all the items from the given inventory. CMD
    /// </summary>
    /// <param name="inventory"></param>
    /// <returns></returns>
    public override void tryToLoot(Inventory inventory) {
      tryToLoot(inventory, out _, out _);
    }

    /// <summary>
    /// override for adding item to player inventory
    /// </summary>
    /// <param name="item"></param>
    /// <param name="inventoryType"></param>
    /// <param name="location"></param>
    /// <param name="succesfullyAddedItem"></param>
    /// <returns></returns>
    public override void tryToAddToInventoryAt(
      Item item,
      PlayerInventorySlot newLocation,
      PlayerInventorySlot oldLocation = default
    ) {
      base.tryToAddItemAt(item, newLocation, oldLocation, out Item succesfullyAddedItem);
      if (succesfullyAddedItem != null) {
        if (newLocation.containingInventory == Player.InventoryTypes.HotBar) {
          Barbelo.EventSystem.notifyChannelOf(
            new ItemsUpdatedEvent(new Coordinate[] { newLocation.pivot }, Player.InventoryTypes.HotBar),
            WorldScapeEventSystem.Channels.LocalPlayerUpdates
          );
        } else if (newLocation.containingInventory == Player.InventoryTypes.GridPack) {
          if (item.quantity > 0) {
            Barbelo.EventSystem.notifyChannelOf(
              new ItemsUpdatedEvent(new Coordinate[] { newLocation.pivot }, Player.InventoryTypes.GridPack),
              Events.WorldScapeEventSystem.Channels.LocalPlayerUpdates
            );
          // if we stacked it but it's off-pivot, remove the old stack and update the pivot's.
          } else {
            playerManager.player.packInventory.getItemStackAt(newLocation.pivot, out Coordinate pivot);
            Barbelo.EventSystem.notifyChannelOf(
              new ItemsUpdatedEvent(new Coordinate[] { pivot }, Player.InventoryTypes.GridPack),
              Events.WorldScapeEventSystem.Channels.LocalPlayerUpdates
            );
          }
        } else if (newLocation.containingInventory == Player.InventoryTypes.Pockets) {
          Barbelo.EventSystem.notifyChannelOf(
            new ItemsUpdatedEvent(new Coordinate[] { newLocation.pivot }, Player.InventoryTypes.Pockets),
            Events.WorldScapeEventSystem.Channels.LocalPlayerUpdates
          );
        }
      }
    }

    /// <summary>
    /// Try to loot override, only used on server internally betweeen managers
    /// </summary>
    /// <returns></returns>
    public override Item[] tryToLoot(IInventory inventory, out Item[] succesfullyAddedUpItems, out (Coordinate, Enum)[] modifiedPivots) {
      Item[] leftovers = base.tryToLoot(inventory, out succesfullyAddedUpItems, out modifiedPivots);

      // send the local player picked up items notification
      foreach (Item item in succesfullyAddedUpItems) {
        Barbelo.EventSystem.notifyChannelOf(
          new ObtainItemsEvent(playerManager.player, item),
          Events.WorldScapeEventSystem.Channels.LocalPlayerUpdates
        );
      }


      // update the local inventory uis if need be
      List<Coordinate> modifiedHotBarPivots = new List<Coordinate>();
      List<Coordinate> modifiedGridPackPivots = new List<Coordinate>();
      foreach ((Coordinate pivot, Enum inventoryType) in modifiedPivots) {
        if (inventoryType is Player.InventoryTypes iT) {
          if (iT == Player.InventoryTypes.GridPack) {
            modifiedGridPackPivots.Add(pivot);
          } else if (iT == Player.InventoryTypes.HotBar) {
            modifiedHotBarPivots.Add(pivot);
          }
        }
      }
      if (modifiedHotBarPivots.Count > 0) {
        Barbelo.EventSystem.notifyChannelOf(
          new ItemsUpdatedEvent(modifiedHotBarPivots.ToArray(), Player.InventoryTypes.HotBar),
          Events.WorldScapeEventSystem.Channels.LocalPlayerUpdates
        );
      }
      if (modifiedGridPackPivots.Count > 0) {
        Barbelo.EventSystem.notifyChannelOf(
          new ItemsUpdatedEvent(modifiedGridPackPivots.ToArray(), Player.InventoryTypes.GridPack),
          Events.WorldScapeEventSystem.Channels.LocalPlayerUpdates
        );
      }

      // return the leftovers
      return leftovers;
    }

    /// <summary>
    /// override for update alerts
    /// </summary>
    public override void removeFromInventoryAt(Player.InventoryTypes inventoryType, Coordinate pivotLocation) {
      removeItemAt(inventoryType, pivotLocation, out Item removedItem);
      if (removedItem != null) {
        Barbelo.EventSystem.notifyChannelOf(
          new ItemsRemovedEvent(new Coordinate[] { pivotLocation }, inventoryType),
          Events.WorldScapeEventSystem.Channels.LocalPlayerUpdates
        );
      }
    }

    #endregion

    #region Events

    /// <summary>
    /// event for player picking up an item
    /// </summary>
    public struct ObtainItemsEvent : IEvent {
      public string name
        => "Player picked up item";

      /// <summary>
      /// The item that was picked up
      /// </summary>
      public Item item {
        get;
      }

      /// <summary>
      /// The player who picked it up
      /// </summary>
      public Player player {
        get;
      }

      /// <summary>
      /// Make an event of this kind
      /// </summary>
      /// <param name="player"></param>
      /// <param name="item"></param>
      public ObtainItemsEvent(Player player, Item item) {
        this.item = item;
        this.player = player;
      }
    }

    /// <summary>
    /// Event for adding items to their pack
    /// </summary>
    public struct ItemsUpdatedEvent : IEvent {
      public string name
        => "Player picked up item";

      /// <summary>
      /// The item that was picked up
      /// </summary>
      public Coordinate[] modifiedPivots {
        get;
      }

      public Player.InventoryTypes updatedInventoryType {
        get;
      }

      /// <summary>
      /// Make an event of this kind
      /// </summary>
      /// <param name="player"></param>
      /// <param name="item"></param>
      public ItemsUpdatedEvent(Coordinate[] modifiedPivots, Player.InventoryTypes updatedInventoryType) {
        this.modifiedPivots = modifiedPivots;
        this.updatedInventoryType = updatedInventoryType;
      }
    }

    /// <summary>
    /// Event for adding items to their pack
    /// </summary>
    public struct ItemsRemovedEvent : IEvent {
      public string name
        => "Player removed item";

      /// <summary>
      /// The item that was picked up
      /// </summary>
      public Coordinate[] modifiedPivots {
        get;
      }

      public Player.InventoryTypes updatedInventoryType {
        get;
      }

      /// <summary>
      /// Make an event of this kind
      /// </summary>
      /// <param name="player"></param>
      /// <param name="item"></param>
      public ItemsRemovedEvent(Coordinate[] modifiedPivots, Player.InventoryTypes updatedInventoryType) {
        this.modifiedPivots = modifiedPivots;
        this.updatedInventoryType = updatedInventoryType;
      }
    }

    #endregion
  }
}