﻿using Mirror;
using SpiritWorld.Game.Controllers;
using SpiritWorld.World.Entities.Creatures;
using UnityEngine;

namespace SpiritWorld.Managers {

  /// <summary>
  /// Used to control players in the overworld
  /// </summary>
  [RequireComponent(typeof(PlayerCharacterInventoryManager))]
  public class PlayerManager : NetworkBehaviour {

    #region Constants

    /// <summary>
    /// The local player's inventory manager.
    /// </summary>
    public PlayerCharacterInventoryManager InventoryManager
      => _InventoryManager ?? (_InventoryManager = GetComponent<PlayerCharacterInventoryManager>());
    PlayerCharacterInventoryManager _InventoryManager;

    /// <summary>
    /// The overworld version of this manager if we have one
    /// TODO: replace with controller calls
    /// </summary>
    public PlayerController Overworld;

    /// <summary>
    /// The controller for this player
    /// </summary>
    public PlayerController Controller
      => Saturn.PlayerControllers[player];

    #endregion

    /// <summary>
    /// The player being controled
    /// </summary>
    public Player player {
      get => _player;
      protected set => _player = value;
    } [SyncVar] Player _player;

    /// <summary>
    /// Set the player being controled
    /// </summary>
    /// <param name="player"></param>
    [Command]
    public virtual void initialize(Player player) {
      this.player = player;
      InventoryManager.initialize(Controller.transform);
    }
  }
}
