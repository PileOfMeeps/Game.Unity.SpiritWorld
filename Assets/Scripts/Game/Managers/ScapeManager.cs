﻿using Mirror;
using SpiritWorld.Game.Data;
using SpiritWorld.Inventories;
using SpiritWorld.Inventories.Items;
using SpiritWorld.World;
using SpiritWorld.World.Terrain.Features;
using SpiritWorld.World.Terrain.TileGrid;
using SpiritWorld.World.Terrain.TileGrid.Generation;
using SpiritWorld.World.Entities.Creatures;
using SpiritWorld.Events;

namespace SpiritWorld.Managers {
  public class ScapeManager : NetworkBehaviour {

    /// <summary>
    /// The current scape
    /// </summary>
    public Scape scape {
      get;
      private set;
    }

    /// <summary>
    /// The ative board of this scape
    /// </summary>
    public TileBoard activeBoard
      => scape.getBoard(activeBoardIndex);


    /// <summary>
    /// The index of the active board
    /// </summary>
    int activeBoardIndex
      = 0;

    /// <summary>
    /// Initialize this world scape
    /// </summary>
    /// <param name="scapeProfile"></param>
    [Command]
    public void initialize(ScapeProfile scapeProfile) {
      scape = new Scape(scapeProfile);

      // set up the scape,
      Biome testForest = new Biome(Biome.Types.RockyForest, 1234);
      scape.addBoard(new RectangularBoard(testForest));
    }

    /// <summary>
    /// Use an item on a tile and see what happens
    /// </summary>
    [Command]
    public void useItemOnTile(Player player, int selectedItemBarSlot, Tile selectedTile, float secondsUsed) {
      // validate the tile and player data by swapping it for the server data
      selectedTile = Barbelo.ActiveScapeManager.activeBoard.get(selectedTile.worldLocation);
      Item selectedItem = Barbelo.PlayerManagers[player].player.hotBarInventory.getItemAt(selectedItemBarSlot);

      // collect the drops
      IInventory drops = null;

      // check if the tile has a resource. If it does, we'll try to mine it
      FeaturesByLayer features 
        = Barbelo.ActiveScapeManager.activeBoard.getFeaturesFor(selectedTile);
      ITool selectedTool = selectedItem is ITool tool ? tool : Player.EmptyHand;
      // try to mine a resource feature.
      if (features != null) {
        if (features.TryGetValue(TileFeature.Layer.Resource, out TileFeature resource)
          && resource.type.CanBeMinedBy(selectedTool, resource.mode)
        ) {
          TileFeature beforeResourceValues = resource;
          TileFeature? workedResource = resource.interact(selectedTool, secondsUsed, out drops);
          // if the tile resource was null, destroy it.
          if (workedResource == null) {

            // remove the tile feature and sent the update to the board to update the feature
            Barbelo.ActiveScapeManager.activeBoard.remove(selectedTile, beforeResourceValues.type.Layer);
            Barbelo.EventSystem.notifyChannelOf(
              new TileFeatureDestroyed(player, selectedTile, beforeResourceValues.type.Layer),
              WorldScapeEventSystem.Channels.TileUpdates
            );
            // if it wasn't we need to update it.
          } else {
            TileFeature updatedResource = (TileFeature)workedResource;
            // if the updated resource doesn't match the old one, we need to update it
            if (!beforeResourceValues.Equals(updatedResource)) {
              Barbelo.ActiveScapeManager.activeBoard.update(selectedTile, updatedResource);
              Barbelo.EventSystem.notifyChannelOf(
                new TileFeatureUpdated(player, selectedTile, updatedResource),
                WorldScapeEventSystem.Channels.TileUpdates
              );
            }
          }
          // shovels can break decorations
        } else if (selectedTool.ToolType == Tool.Type.Shovel && features.TryGetValue(TileFeature.Layer.Decoration, out TileFeature decoration)) {
          TileFeature beforeShoveledValues = decoration;
          TileFeature? shoveledDecoration = decoration.interact(selectedTool, secondsUsed, out drops);
          if (shoveledDecoration == null) {
            // remove the tile feature and sent the update to the board to update the feature
            // TODO: move these calls to a [Command] function in the board manager
            Barbelo.ActiveScapeManager.activeBoard.remove(selectedTile, beforeShoveledValues.type.Layer);
            Barbelo.EventSystem.notifyChannelOf(
              new TileFeatureDestroyed(player, selectedTile, beforeShoveledValues.type.Layer),
              WorldScapeEventSystem.Channels.TileUpdates
            );
          }
        }
      }

      // If the tile feature or decoration had drops, give them to the player or drop them
      if (drops != null) {
        Item[] leftoverDrops = Barbelo.PlayerManagers[player].InventoryManager.tryToLoot(drops, out _, out _);
        if (leftoverDrops != null) {
          /// alert the world of a drop event.
          Barbelo.EventSystem.notifyChannelOf(
            new TileFeatureDropsLeftover(
              selectedTile,
              leftoverDrops,
              Barbelo.ActiveScapeManager.activeBoard[selectedTile.parentChunkKey].add(leftoverDrops)
            ),
            Events.WorldScapeEventSystem.Channels.TileUpdates
          );
        }
      }
    }

    #region Events

    /// <summary>
    /// An event to send off to let managers know a tile feature for a certain layer has been changed
    /// </summary>
    public struct TileFeatureUpdated : IEvent {

      /// <summary>
      /// Name
      /// </summary>
      public string name
        => "Tile Feature Mode Changed";

      /// <summary>
      /// The tile that was updated
      /// </summary>
      public Tile updatedTile {
        get;
      }

      /// <summary>
      /// The feature that was updated
      /// </summary>
      public TileFeature updatedFeature {
        get;
      }

      /// <summary>
      /// The player who destroyed the feature
      /// </summary>
      public Player player {
        get;
      }

      /// <summary>
      /// Make a new event
      /// </summary>
      /// <param name="tile"></param>
      /// <param name="feature"></param>
      public TileFeatureUpdated(Player by, Tile tile, TileFeature feature) {
        player = by;
        updatedTile = tile;
        updatedFeature = feature;
      }
    }

    /// <summary>
    /// An event to send off to let managers know a tile feature for a certain layer has been destroyed
    /// </summary>
    public struct TileFeatureDestroyed : IEvent {

      /// <summary>
      /// Name
      /// </summary>
      public string name
        => "Tile Feature Destroyed";

      /// <summary>
      /// The feature that was updated
      /// </summary>
      public Tile updatedTile {
        get;
      }

      /// <summary>
      /// The tile that was updated
      /// </summary>
      public TileFeature.Layer removedLayer {
        get;
      }

      /// <summary>
      /// The player who destroyed the feature
      /// </summary>
      public Player player {
        get;
      }

      /// <summary>
      /// Make a new event
      /// </summary>
      /// <param name="tile"></param>
      /// <param name="feature"></param>
      public TileFeatureDestroyed(Player by, Tile tile, TileFeature.Layer removedLayer) {
        player = by;
        updatedTile = tile;
        this.removedLayer = removedLayer;
      }
    }

    /// <summary>
    /// An event to send off to let managers know a tile feature was mined and some drops were leftover
    /// </summary>
    public struct TileFeatureDropsLeftover : IEvent {

      /// <summary>
      /// Name
      /// </summary>
      public string name
        => "Tile Feature Drops Leftover";

      /// <summary>
      /// The feature that was updated
      /// </summary>
      public Tile updatedTile {
        get;
      }

      /// <summary>
      /// The leftover drops
      /// </summary>
      public Item[] drops {
        get;
      }

      /// <summary>
      /// The id of the drop in the chunk it's managed by
      /// </summary>
      public int[] dropIds {
        get;
      }

      /// <summary>
      /// Make a new event
      /// </summary>
      /// <param name="tile"></param>
      /// <param name="feature"></param>
      public TileFeatureDropsLeftover(Tile tile, Item[] drops, int[] dropIds) {
        updatedTile = tile;
        this.drops = drops;
        this.dropIds = dropIds;
      }
    }

    #endregion
  }
}
