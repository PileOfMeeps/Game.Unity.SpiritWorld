﻿using SpiritWorld.Inventories.Items;
using SpiritWorld.World.Entities.Creatures;
using SpiritWorld.World.Terrain.TileGrid;
using System;
using System.Collections.Generic;

namespace SpiritWorld.Inventories {

  /// <summary>
  /// The inventory for the alchemy grid
  /// </summary>
  public class AlchemyGrid : GridBasedInventory {

    /// <summary>
    /// The iten stack ids indexed by the axial key of the item slot they're in
    /// valid but empty slots contain EmptyGridSlot(-1)
    /// </summary>
    Dictionary<Coordinate, int> axialStackSlots
      = new Dictionary<Coordinate, int>();

    /// <summary>
    /// The sources for the items placed in this grid, so we can try to put them back when it's closed.
    /// </summary>
    Dictionary<Coordinate, (Player.InventoryTypes, Coordinate)> itemSources
      = new Dictionary<Coordinate, (Player.InventoryTypes, Coordinate)>();

    /// <summary>
    /// Make an alchemy grid
    /// </summary>
    public AlchemyGrid() : base((7, 7)) {
      // populate a heagonal grid of valid item slots
      HexGridShaper.Hexagon(3, axialKey => {
        axialStackSlots.Add(axialKey, EmptyGridSlot);
      });
    }

    /// <summary>
    /// Add an item with a source so we know where to try to return it to
    /// </summary>
    /// <param name="item"></param>
    /// <param name="axialKey"></param>
    /// <param name="sourceLocation"></param>
    /// <param name="succesfullyAddedItem"></param>
    /// <returns></returns>
    public Item tryToAdd(Item item, Coordinate axialKey, (Player.InventoryTypes, Coordinate) sourceLocation, out Item succesfullyAddedItem) {
      Item leftovers = tryToAdd(item, axialKey, out succesfullyAddedItem, out bool wasSwapped);
      // if the item was swapped for another, we'll still try to put it back into the original empty slot the first one came from
      if (!wasSwapped) {
        itemSources.Add(axialKey, sourceLocation);
      }

      return leftovers;
    }

    /// <summary>
    /// remove each item and preform an action on it with it's source inventory info
    /// </summary>
    /// <param name="action"></param>
    public void removeEach(Action<Item, (Player.InventoryTypes inventory, Coordinate gridLocation)?> action) {
      foreach ((Coordinate axialKey, _) in axialStackSlots) {
        Item removedItem = removeAt(axialKey);
        if (removedItem != null) {
          if (itemSources.TryGetValue(axialKey, out var itemSource)) {
            action(removedItem, itemSource);
          } else {
            action(removedItem, null);
          }
        }
      }
    }

    #region IInventory

    /// <summary>
    /// Try to add an item to the axial slot location of the alchemy grid
    /// </summary>
    /// <param name="item"></param>
    /// <param name="location"></param>
    /// <param name="successfullyAddedItem"></param>
    /// <returns></returns>
    public override Item tryToAdd(Item item, Coordinate location, out Item successfullyAddedItem) {
      return tryToAdd(item, location, out successfullyAddedItem, out _);
    }

    /// <summary>
    /// This will only place a given item in the first empty slot, AlchemyGrid does not support auto-stacking!
    /// </summary>
    /// <param name="item"></param>
    /// <param name="successfullyAddedItem"></param>
    /// <param name="modifiedStackPivots"></param>
    /// <returns></returns>
    public override Item tryToAdd(Item item, out Item successfullyAddedItem, out Coordinate[] modifiedStackPivots) {
      foreach ((Coordinate axialKey, int stackId) in axialStackSlots) {
        if (stackId == EmptyGridSlot) {
          modifiedStackPivots = new Coordinate[] { axialKey };
          successfullyAddedItem = item;
          addStackToSlot(item, axialKey);

          return null;
        }
      }

      modifiedStackPivots = new Coordinate[0];
      successfullyAddedItem = null;
      return item;
    }

    /// <summary>
    /// Remove the item at the given location
    /// </summary>
    /// <param name="pivotLocation"></param>
    /// <returns></returns>
    public override Item removeAt(Coordinate pivotLocation) {
      Item removedItem = base.removeAt(pivotLocation);
      if (removedItem != null) {
        axialStackSlots.Remove(pivotLocation);
        itemSources.Remove(pivotLocation);
      }

      return removedItem;
    }

    /// <summary>
    /// tryto add with wasswapped utility
    /// </summary>
    /// <param name="item"></param>
    /// <param name="location"></param>
    /// <param name="successfullyAddedItem"></param>
    /// <param name="wasSwapped"></param>
    /// <returns></returns>
    Item tryToAdd(Item item, Coordinate location, out Item successfullyAddedItem, out bool wasSwapped) {
      successfullyAddedItem = null;
      wasSwapped = false;
      Item leftovers = item;
      if (axialStackSlots.TryGetValue(location, out int stackId)) {
        // if it's a valid empty slot, just add it
        if (stackId == -1) {
          successfullyAddedItem = item;
          axialStackSlots[location] = addStackToSlot(item, location);
          return null;
        }
        // if it's not an empty slot...
        Item currentItemStack = stacks[stackId];
        if (!currentItemStack.isFull && currentItemStack.canStackWith(item)) {
          leftovers = addToStack(leftovers, location, out successfullyAddedItem);
        } else {
          leftovers = tryToSwapOut(location, leftovers, out successfullyAddedItem);
          wasSwapped = successfullyAddedItem != null;
        }
      }

      return leftovers;
    }

    #endregion
  }
}
