﻿using Mirror;
using SpiritWorld.Inventories.Items;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SpiritWorld.Inventories {

  /// <summary>
  /// basic inventory class for storing basic items in single stacks.
  /// for stuff like drops
  /// </summary>
  public class BasicInventory : Inventory {

    /// <summary>
    /// The items stored
    /// </summary>
    Dictionary<Item.Type, Item> items 
      = new Dictionary<Item.Type, Item>();

    /// <summary>
    /// the basic inventory
    /// </summary>
    public BasicInventory() {}

    /// <summary>
    /// Create a basic inventory from a list in the format:
    /// ITemId:quantity,ItemId,quantity
    /// </summary>
    /// <param name="v"></param>
    public BasicInventory(string inventoryString) {
      string[] itemStacks = inventoryString.Split(',');
      foreach(string itemString in itemStacks) {
        tryToAdd(new Item(itemString), out _);
      }
    }

    /// <summary>
    /// Generic add an item to the inventory
    /// </summary>
    /// <param name="item"></param>
    public override Item tryToAdd(Item item, out Item successfullyAddedItem) {
      Item existingItem = items[item.type];
      if (existingItem != null) {
        return existingItem.addToStack(item, out successfullyAddedItem);
      } else {
        items[item.type] = item;
        successfullyAddedItem = item;

        return null;
      }
    }

    /// <summary>
    /// Remove an item.
    /// Default removes all of the item.
    /// </summary>
    /// <return>returns the removed item stack or null if no items were removed</return>>
    public override Item[] remove(Item item, int? quantity = null) {
      Item existingItem = items[item.type];
      // if the item exists, remove and return what we want from the stack
      if (quantity != null && quantity > 0) {
        if (existingItem != null) {
          Item removedItems = existingItem.removeFromStack((byte)quantity);
          // remove the item if there's none left
          if (existingItem.quantity == 0) {
            items.Remove(item.type);
          }
          // return the removed items if there are any
          if (removedItems.quantity != 0) {
            return new Item[] { removedItems };
          }
          return null;
        // else we removed nothing, so return null
        } else return null;
      // if it's -1 or 0 return the whole stack
      } else if (quantity == null || quantity != 0) {
        var itemsRemoved = existingItem;
        items.Remove(item.type);

        return new Item[] { itemsRemoved };
      // if we're not returning anything return nothing
      } else {
        return null;
      }
    }

    /// <summary>
    /// Empty it
    /// </summary>
    /// <returns></returns>
    public override Item[] empty() {
      return items.Values.ToArray();
    }


    /// <summary>
    /// Empty this inventory into another one
    /// TODO: make sure this deletes items that hit 0 from this inventory
    /// </summary>
    /// <returns>leftovers that don't fit</returns>
    public override Item[] emptyInto(IInventory otherInventory, out Item[] successfullyAddedItems) {
      List<Item> addedItems = new List<Item>();
      List<Item> leftovers = new List<Item>();
      foreach(Item stack in items.Values) {
        leftovers.Add(otherInventory.tryToAdd(stack, out Item addedItem));
        // TODO remove emptied stacks
        if (addedItem != null) {
          addedItems.Add(addedItem);
        }
      }

      successfullyAddedItems = addedItems.ToArray();
      return leftovers.ToArray();
    }

    /// <summary>
    /// Make a copy of this inventory
    /// </summary>
    public override IInventory copy() {
      BasicInventory copy = new BasicInventory();
      foreach(Item stack in items.Values) {
        copy.items.Add(stack.type, stack.copy());
      }

      return copy;
    }

    /// <summary>
    /// find the given items
    /// </summary>
    public override Item[] search(Func<Item, bool> query) {
      List<Item> matches = new List<Item>();
      foreach(Item item in items.Values) {
        if (query(item)) {
          matches.Add(item);
        }
      }

      return matches.ToArray();
    }

    /// <summary>
    /// Inventory Contents
    /// </summary>
    public override string ToString() {
      string text = "I{";
      foreach(Item item in items.Values) {
        text += $"[{item.type.Name}:{item.quantity}], ";
      }
      text.Trim(new char[] {' ', ','});
      return text + "}";
    }

    #region Serialization

    /// <summary>
    /// deserialization
    /// </summary>
    /// <param name="reader"></param>
    public BasicInventory(NetworkReader reader) : base(reader) { }

    /// <summary>
    /// serialize
    /// </summary>
    /// <param name="writer"></param>
    public override void serializeTo(NetworkWriter writer) {
      writer.Write(items);
      base.serializeTo(writer);
    }

    /// <summary>
    /// deserialize
    /// </summary>
    /// <param name="reader"></param>
    protected override void deserializeFrom(NetworkReader reader) {
      reader.Read<Dictionary<Item.Type, Item>>();
      base.deserializeFrom(reader);
    }

    #endregion
  }
}
