﻿using Mirror;
using SpiritWorld.Inventories.Items;
using System;
using System.Collections.Generic;

namespace SpiritWorld.Inventories {

  /// <summary>
  /// An unshaped inventory in the shape of a 2D grid
  /// </summary>
  public class GridPack : GridBasedInventory {

    /// <summary>
    /// The slot (pivot slot if it's shaped) locations for each stack
    /// </summary>
    protected Dictionary<int, Coordinate> stackPivotLocations
      = new Dictionary<int, Coordinate>();

    /// <summary>
    /// Make a new grid pack
    /// </summary>
    /// <param name="dimensions"></param>
    public GridPack((int x, int y) dimensions) : base(dimensions) { }

    /// <summary>
    /// Try to add the item at the given slot and return any leftovers
    /// </summary>
    public override Item tryToAdd(Item item, Coordinate inventorySlot, out Item successfullyAddedItems) {
      // first check if we can stack with the item already there
      Item currentStack = getItemStackAt(inventorySlot);
      if (currentStack != null) {
        // if it matches, stack it
        if (!currentStack.isFull && currentStack.canStackWith(item)) {
          return addToStack(item, inventorySlot, out successfullyAddedItems);
        }

        // if it doesn't match, try to swap it out
        return tryToSwapOut(inventorySlot, item, out successfullyAddedItems);
        // if there's nothing there just place it
      } else {
        successfullyAddedItems = item;
        int stackId = addStackToSlot(item, inventorySlot);
        stackPivotLocations[stackId] = inventorySlot;

        return null;
      }
    }

    /// <summary>
    /// Try to add the item to the first open slot
    /// </summary>
    /// <param name="item"></param>
    /// <param name="successfullyAddedItem"></param>
    /// <param name="modifiedSlotLocations"></param>
    /// <returns></returns>
    public override Item tryToAdd(Item item, out Item successfullyAddedItems, out Coordinate[] modifiedSlotLocations) {
      Item itemsLeftToAdd = item;
      Item itemsSuccessfullyAdded = null;
      List<Coordinate> modifiedSlots = new List<Coordinate>();

      // first go through and try to stack everything we can
      forEach((stackLocation, existingItemStack, stackId) => {
        if (!existingItemStack.isFull && existingItemStack.canStackWith(itemsLeftToAdd)) {
          modifiedSlots.Add(stackLocation);
          itemsLeftToAdd = existingItemStack.addToStack(itemsLeftToAdd, out Item itemsAdded);
          if (itemsAdded != null) {
            itemsSuccessfullyAdded = itemsSuccessfullyAdded == null
              ? itemsAdded
              : itemsSuccessfullyAdded.addToStack(itemsAdded, out _);
          }
        }

        /// stop if it's empty
        if (itemsLeftToAdd == null) {
          return false;
        }

        return true;
      });


      /// try to find an open slot to stick it in, or a matching item 
      Coordinate.Zero.until(dimensions, gridSlotLocation => {
        int initalStackSize = itemsLeftToAdd.quantity;
        if (getItemStackAt(gridSlotLocation) == null) {
          itemsLeftToAdd = tryToAdd(itemsLeftToAdd, gridSlotLocation, out Item itemsAdded);
          if (itemsAdded != null) {
            itemsSuccessfullyAdded = itemsSuccessfullyAdded == null
              ? itemsAdded
              : itemsSuccessfullyAdded.addToStack(itemsAdded, out _);
          }
        }
        if (initalStackSize != itemsLeftToAdd?.quantity) {
          modifiedSlots.Add(gridSlotLocation);
        }
        if (itemsLeftToAdd == null) {
          return false;
        }

        return true;
      });

      modifiedSlotLocations = modifiedSlots.ToArray();
      successfullyAddedItems = itemsSuccessfullyAdded;
      return itemsLeftToAdd;
    }

    /// <summary>
    /// Do something on each item stack given it's pivot location
    /// </summary>
    /// <param name="action">item pivot location, item stack, stack id</param>
    public virtual void forEach(Func<Coordinate, Item, int, bool> action) {
      for (int stackId = 0; stackId < stacks.Count; stackId++) {
        if (stacks[stackId] is Item stack && stack != null) {
          if (!action(stackPivotLocations[stackId], stack, stackId))
            return;
        }
      }
    }

    #region Serialization

    /// <summary>
    /// deserialization constructor
    /// </summary>
    /// <param name="reader"></param>
    public GridPack(NetworkReader reader) : base(reader) { }

    /// <summary>
    /// serialize
    /// </summary>
    /// <param name="writer"></param>
    public override void serializeTo(NetworkWriter writer) {
      writer.Write(stackPivotLocations);
      base.serializeTo(writer);
    }

    /// <summary>
    /// deserialize
    /// </summary>
    /// <param name="reader"></param>
    protected override void deserializeFrom(NetworkReader reader) {
      stackPivotLocations = reader.Read<Dictionary<int, Coordinate>>();
      base.deserializeFrom(reader);
    }

    #endregion
  }
}
