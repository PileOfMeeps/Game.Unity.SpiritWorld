﻿using Mirror;
using SpiritWorld.Inventories.Items;
using System;
using System.Collections.Generic;

namespace SpiritWorld.Inventories {
  public abstract class Inventory : IInventory {

    /// <summary>
    /// The id of the profile, should be UUID
    /// </summary>
    public string id {
      get;
      private set;
    }

    /// <summary>
    /// The user set name of the profile
    /// </summary>
    public string name {
      get;
      private set;
    }

    /// <summary>
    /// base constructor
    /// </summary>
    public Inventory() { }

    #region Implimented IInventory

    /// <summary>
    /// Add multuple items
    /// </summary>
    /// <returns>leftovers that don't fit</returns>
    public Item[] tryToAdd(Item[] items, out Item[] successfullyAddedItems) {
      List<Item> successfullyAddedItemList = new List<Item>();
      List<Item> leftovers = new List<Item>();
      foreach(Item item in items) {
        leftovers.Add(tryToAdd(item, out Item successfullyAddedItemStack));
        if (successfullyAddedItemStack != null) {
          successfullyAddedItemList.Add(successfullyAddedItemStack);
        }
      }

      successfullyAddedItems = successfullyAddedItemList.ToArray();
      return leftovers.ToArray();
    }

    #endregion

    #region Abstract IInventory

    public abstract IInventory copy();
    public abstract Item[] empty();
    public abstract Item[] emptyInto(IInventory otherInventory, out Item[] successfullyAddedItems);
    public abstract Item[] remove(Item item, int? quantity = null);
    public abstract Item[] search(Func<Item, bool> query);
    public abstract Item tryToAdd(Item item, out Item successfullyAddedItem);

    #endregion

    #region Serialization

    /// <summary>
    /// deserialization constructor
    /// </summary>
    public Inventory(NetworkReader reader) {
      deserializeFrom(reader);
    }

    /// <summary>
    /// serialize this inventory data to the writier, call this at the end of overrides
    /// </summary>
    public virtual void serializeTo(NetworkWriter writer) {
      writer.WriteString(id);
      writer.WriteString(name);
    }

    /// <summary>
    /// base deserialize, call this at the end of overrides
    /// </summary>
    protected virtual void deserializeFrom(NetworkReader reader) {
      id = reader.ReadString();
      name = reader.ReadString();
    }

    #endregion
  }

  #region Serialzation ReadWriter

  /// <summary>
  /// serialized for the different kinds of inventories
  /// </summary>
  public static class InventoryReadWriter {

    /// <summary>
    /// serialize by inventory type
    /// </summary>
    /// <param name="writer"></param>
    /// <param name="inventory"></param>
    public static void WriteInventory(this NetworkWriter writer, Inventory inventory) {
      writer.WriteString(inventory.GetType().Name);
      inventory.serializeTo(writer);
    }

    /// <summary>
    /// Read the correct inventory type in
    /// </summary>
    /// <param name="reader"></param>
    /// <returns></returns>
    public static Inventory ReadInventory(this NetworkReader reader) {
      string inventoryType = reader.ReadString();
      Inventory inventory;
      if (inventoryType == typeof(BasicInventory).Name) {
        inventory = new BasicInventory(reader);
      } else if (inventoryType == typeof(ShapedPack).Name) {
        inventory = new ShapedPack(reader);
      } else if (inventoryType == typeof(ItemBar).Name) {
        inventory = new ItemBar(reader);
      } else if (inventoryType == typeof(SlotBasedInventory).Name) {
        inventory = new SlotBasedInventory(reader);
      } else if (inventoryType == typeof(StackedInventory).Name) {
        inventory = new StackedInventory(reader);
      } else {
        throw new InvalidOperationException("Cannot deserialize inventory of type " + inventoryType);
      }

      return inventory;
    }
  }

  #endregion
}
