﻿using Mirror;
using SpiritWorld.Inventories.Items;

namespace SpiritWorld.Inventories {

  /// <summary>
  /// A linear bar of items, capable of expanding slots like folders
  /// </summary>
  public class ItemBar : SlotBasedInventory {

    /// <summary>
    /// How many expandable slots can one bar slot hold
    /// </summary>
    int maxSlotDepth;

    /// <summary>
    /// The mad bar sise
    /// </summary>
    int maxBarSize;

    #region Initialization

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="size"></param>
    public ItemBar(int slotCount, int maxSlotDepth = 1, int activeSlotCount = 0, Item[] items = null) : base(activeSlotCount < 1 ? slotCount : activeSlotCount) {
      maxBarSize = slotCount;
      this.maxSlotDepth = maxSlotDepth;
      if (items != null) {
        int slotIndex = 0;
        foreach(Item item in items) {
          tryToAdd(item, (slotIndex++, 0), out _);
        }
      }
    }

    /// <summary>
    /// base constructor
    /// </summary>
    public ItemBar() : base(20) {}

    #endregion

    #region Inventory Modifications

    /// <summary>
    /// try to add or remove an available slot from the bar, and add it to the pocket size
    /// TODO: this needs to be fixed up
    /// </summary>
    /// <returns>the items moved to the pockets or bar depending on size change direction</returns>
    public bool incrementBarSize(out Item removedItemStack, bool increase = true) {
      removedItemStack = null;
      int slotCountModifier = increase ? 1 : -1;
      int newActiveBarSize = dimensions.x + slotCountModifier;

      // could not change
      if (newActiveBarSize <= 0 || newActiveBarSize >= maxBarSize) {
        return false;
      }
      // change dimensions
      dimensions = (dimensions.x + slotCountModifier, dimensions.y);

      // if we're shrinking, get any removed items
      if (slotCountModifier < 0) {
        removedItemStack = removeAt(dimensions.x);
      }

      return true;
    }

    #endregion

    #region Content Modification

    /// <summary>
    /// Try to add an item to the bar at the given slot.
    /// TODO: doesn't work for pockets, add functionality
    /// </summary>
    /// <param name="item">Item to add</param>
    /// <param name="barAndDeepSlot">bar slot, and depth slot to add it to (0 base)</param>
    /// <param name="successfullyAddedItem">The item that was added successfully if there was one</param>
    /// <returns>Any items replaced or removed from the slot in order to place the new one</returns>
    public override Item tryToAdd(Item item, Coordinate barAndDeepSlot, out Item successfullyAddedItem) {
      (int barSlot, int deepSlot) = barAndDeepSlot;

      /// if it's within the current bar
      if (barSlot < dimensions.x) {
        // if it's the main bar slot or we've expanded this far already into the deep spots, just swap out the item.
        if (deepSlot == 0 || stackSlotGrid[barSlot].Length > deepSlot) {
          return tryToSwapOut(barAndDeepSlot, item, out successfullyAddedItem);
        // if we need to expand by one
        } else if (deepSlot < maxSlotDepth) {
          addStackToSlot(item, (barSlot, deepSlot));
          successfullyAddedItem = item;
          return null;
        }
      }

      /// failed to add the item, return it as leftover
      successfullyAddedItem = null;
      return item;
    }

    #endregion

    #region Serialization

    /// <summary>
    /// deserialization
    /// </summary>
    /// <param name="slotCount"></param>
    public ItemBar(NetworkReader reader) : base(reader) { }

    /// <summary>
    /// serialize
    /// </summary>
    /// <param name="writer"></param>
    public override void serializeTo(NetworkWriter writer) {
      writer.WriteInt32(slotCount);
      writer.WriteInt32(maxSlotDepth);
      base.serializeTo(writer);
    }

    /// <summary>
    /// deserialize
    /// </summary>
    /// <param name="reader"></param>
    protected override void deserializeFrom(NetworkReader reader) {
      maxBarSize = reader.ReadInt32();
      maxSlotDepth = reader.ReadInt32();

      base.deserializeFrom(reader);
    }

    #endregion
  }
}