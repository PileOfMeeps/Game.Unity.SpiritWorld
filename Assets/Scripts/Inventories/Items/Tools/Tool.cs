﻿using System.Collections.Generic;

namespace SpiritWorld.Inventories.Items {

  /// <summary>
  /// Just tool constants idk
  /// </summary>
  public static class Tool {

    /// <summary>
    /// Types of tools that are useable
    /// </summary>
    public enum Type {
      None = 0,
      Axe,
      Shovel,
      Pickaxe,
      Any = 36,
      Multi = 100
    }

    /// <summary>
    /// Tool shortcut items
    /// </summary>
    public static Dictionary<Type, Item> Shortcuts = new Dictionary<Type, Item>() {
      {
        Type.Multi,
        new Item(Item.Types.AutoToolShortcut)
      }
    };

    /// <summary>
    /// A small struct used to store tool requirements for harvesting or mining
    /// </summary>
    public struct Requirement {
      /// <summary>
      /// The type of tool needed
      /// </summary>
      public Type ToolType {
        get;
      }

      /// <summary>
      /// The minimum level of the tool needed
      /// </summary>
      public int MinimumLevel {
        get;
      }

      /// <summary>
      /// Make a new requirement
      /// </summary>
      /// <param name="type"></param>
      /// <param name="minlevel"></param>
      public Requirement(Type type, int minlevel = 0) {
        ToolType = type;
        MinimumLevel = minlevel;
      }
    }
  }
}
