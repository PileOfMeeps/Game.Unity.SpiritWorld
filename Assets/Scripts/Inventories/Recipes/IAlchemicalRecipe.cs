﻿using SpiritWorld.Inventories.Items;

namespace SpiritWorld.Inventories.Recipes {
  public interface IAlchemicalRecipe {

    /// <summary>
    /// Get if this recipie matches the given set of grid contents.
    /// </summary>
    /// <param name="alchemyGridContents"></param>
    /// <returns></returns>
    bool matches(AlchemyGrid alchemyGridContents);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="contentsOf"></param>
    /// <returns></returns>
    bool canBeMadeFrom(IInventory inventory);

    /// <summary>
    /// Craft this item using the items in the given inventory
    /// </summary>
    /// <param name="alchemyGrid"></param>
    /// <returns></returns>
    Item craftWith(IInventory alchemyGrid);
  }
}