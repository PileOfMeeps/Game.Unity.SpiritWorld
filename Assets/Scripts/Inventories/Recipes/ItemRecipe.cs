﻿using SpiritWorld.Inventories.Items;

namespace SpiritWorld.Inventories.Recipes {

  /// <summary>
  /// A base class for alchemical recipes that produces an item
  /// </summary>
  public abstract class ItemRecipe : IAlchemicalRecipe {
    public abstract bool canBeMadeFrom(IInventory inventory);

    public Item craftWith(IInventory alchemyGrid) {
      throw new System.NotImplementedException();
    }

    public bool matches(AlchemyGrid alchemyGridContents) {
      throw new System.NotImplementedException();
    }
  }
}