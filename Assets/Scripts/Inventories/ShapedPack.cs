﻿using Mirror;
using SpiritWorld.Inventories.Items;
using System;

namespace SpiritWorld.Inventories {

  /// <summary>
  /// A shaped-item inventory in the shape of a 2D grid.
  /// </summary>
  public class ShapedPack : GridPack {

    /// <summary>
    /// Make a shaped item pack of the given dimensions with starting items
    /// </summary>
    /// <param name="dimensions"></param>
    public ShapedPack((int x, int y) dimensions, (Item, Coordinate)[] initialValues) : base(dimensions) {
      foreach(var (item, addLocation) in initialValues) {
        tryToAdd(item, addLocation, out _);
      }
    }

    /// <summary>
    /// Try to add an item to the grid, centered on the given location
    /// </summary>
    /// <param name="item">The item we're trying to add</param>
    /// <param name="location">the place the center/pivot of the shaped item icon is placed on the grid</param>
    /// <param name="successfullyAddedItem">Any items sucessfully added</param>
    /// <returns>Leftovers/items not added or returned.</returns>
    public override Item tryToAdd(Item item, Coordinate location, out Item successfullyAddedItem) {
      Item leftoverStack = null;
      successfullyAddedItem = null;

      // check the placed center location stacks first
      Item pivotStack = getItemStackAt(location);
      // if it's null, check to see if the whole shape worth of spaces are empty and add it if so
      if (pivotStack == null) {
        bool allStacksAreEmpty = true;
        // check the other spaces around the pivot and make sure they're also empty.
        Coordinate itemPivot = item.type.ShapePivot;
        Coordinate.Zero.until((item.type.Shape.GetLength(0), item.type.Shape.GetLength(1)), offset => {
          // from bottom left => right top
          Coordinate currentGridLocation = location + (offset - itemPivot);
          // make sure the grid location exists
          if (!currentGridLocation.isWithin(Coordinate.Zero, dimensions)) {
            leftoverStack = item;
            allStacksAreEmpty = false;
            return false;
          }
          // we can skip the middle one (pivot type) too because we already checked it.
          if (item.type.Shape[offset.x, offset.z] == Item.Type.ShapeBlocks.Solid) {
            Item blockStack = getItemStackAt(currentGridLocation);
            // if the blockstack is occupied, abort.
            if (blockStack != null) {
              leftoverStack = item;
              allStacksAreEmpty = false;
              return false;
            }
          }

          return true;
        });

        /// if all the stacks were empty, add the shaped item
        if (allStacksAreEmpty) {
          successfullyAddedItem = item;
          addShapedItemStack(item, location);
        }
      /// if it can stack, stack em
      } else if (item.canStackWith(pivotStack)) {
        leftoverStack = pivotStack.addToStack(item, out successfullyAddedItem);
      /// try to swap it for the other item if they're the same shape
      } else if (item.type.Shape == pivotStack.type.Shape) {
        leftoverStack = tryToSwapOut(location, item, out successfullyAddedItem);
      /// if we couldn't stack or swap it, return the item
      } else {
        leftoverStack = item;
      }

      return leftoverStack;
    }

    /// <summary>
    /// Remove the item at the given coordinate location
    /// </summary>
    /// <param name="pivotLocation"></param>
    /// <returns></returns>
    public override Item removeAt(Coordinate pivotLocation) {
      Item removedItem = getItemStackAt(pivotLocation, out int stackId);
      if (stackId != EmptyGridSlot) {
        clearStack(stackId);
      }

      removeShapedItemStack(removedItem, pivotLocation);
      stackPivotLocations.Remove(stackId);

      return removedItem;
    }

    /// <summary>
    /// Get the item stack and it's pivot
    /// </summary>
    /// <param name="location"></param>
    /// <param name="pivot"></param>
    /// <returns></returns>
    public Item getItemStackAt(Coordinate location, out Coordinate pivot) {
      return getItemStackAt(location, out pivot, out _);
    }

    /// <summary>
    /// Get the item stack and it's pivot
    /// </summary>
    /// <param name="location"></param>
    /// <param name="pivot"></param>
    /// <returns></returns>
    public Item getItemStackAt(Coordinate location, out Coordinate pivot, out int stackId) {
      Item item = getItemStackAt(location, out stackId);
      pivot = stackId != EmptyGridSlot
        ? stackPivotLocations[stackId]
        : new Coordinate(EmptyGridSlot, EmptyGridSlot);
      return item;
    }

    /// <summary>
    /// Add a shaped item at the given pivot location
    /// </summary>
    /// <param name="item"></param>
    /// <param name="location"></param>
    void removeShapedItemStack(Item removedItem, Coordinate location) {
      Coordinate itemPivot = removedItem.type.ShapePivot;
      Coordinate.Zero.until((removedItem.type.Shape.GetLength(0), removedItem.type.Shape.GetLength(1)), offset => {
        // from bottom left => right top
        Coordinate currentGridLocation = location + (offset - itemPivot);
        stackSlotGrid[currentGridLocation.x][currentGridLocation.y] = EmptyGridSlot;
      });
    }

    /// <summary>
    /// Do something on each item stack given it's pivot location
    /// </summary>
    /// <param name="action">item pivot location, item stack, stack id</param>
    public void forEach(Action<Coordinate, Item, int> action) {
      for(int stackId = 0; stackId < stacks.Count; stackId++) {
        if (stacks[stackId] is Item stack && stack != null) {
          action(stackPivotLocations[stackId], stack, stackId);
        }
      }
    }

    /// <summary>
    /// Do something on each item stack given it's pivot location
    /// </summary>
    /// <param name="action">item pivot location, item stack, stack id</param>
    public override void forEach(Func<Coordinate, Item, int, bool> action) {
      for(int stackId = 0; stackId < stacks.Count; stackId++) {
        if (stacks[stackId] is Item stack && stack != null) {
          if (!action(stackPivotLocations[stackId], stack, stackId))
            return;
        }
      }
    }

    /// <summary>
    /// Add a shaped item at the given pivot location
    /// </summary>
    /// <param name="item"></param>
    /// <param name="location"></param>
    void addShapedItemStack(Item item, Coordinate location) {
      int? stackId = null;
      Coordinate itemPivot = item.type.ShapePivot;
      Coordinate.Zero.until((item.type.Shape.GetLength(0), item.type.Shape.GetLength(1)), offset => {
        // from bottom left => right top
        // we can skip the middle one (pivot type) too because we already checked it.
        Coordinate currentGridLocation = location + (offset - itemPivot);
        if (item.type.Shape[offset.x, offset.z] != Item.Type.ShapeBlocks.Empty) {
          // add the item to a new stack first, then use that stack id to populate the slot grid
          if (stackId == null) {
            stackId = addStackToSlot(item, currentGridLocation);
          } else {
            setStackToSlot((int)stackId, currentGridLocation);
          }
        }
      });

      // add the pivot
      stackPivotLocations.Add((int)stackId, location);
    }

    #region Serialization

    /// <summary>
    /// deserialization constructor
    /// </summary>
    /// <param name="reader"></param>
    public ShapedPack(NetworkReader reader) : base(reader) { }

    #endregion
  }
}
