﻿using Mirror;
using SpiritWorld.Inventories.Items;
using System.Collections.Generic;

namespace SpiritWorld.Inventories {

  /// <summary>
  /// An inventory in a strait line witb unshaped slots
  /// </summary>
  public class SlotBasedInventory : GridBasedInventory {

    /// <summary>
    /// how many slots are in the item bar
    /// </summary>
    public int slotCount
      => dimensions.x;

    /// <summary>
    /// base constructor
    /// </summary>
    /// <param name="slotCount"></param>
    public SlotBasedInventory(int slotCount) : base((slotCount, 1)) { }

    #region Data Accessors

    /// <summary>
    /// Check the item at the given index of the item bar without changing it
    /// NOT USED TO REMOVE
    /// </summary>
    /// <param name="slotIndex"></param>
    /// <returns></returns>
    public Item getItemAt(int slotIndex) {
      return getItemStackAt((slotIndex, 0));
    }

    #endregion

    #region Content Modification

    /// <summary>
    /// Add an item and return the modifieid pivot ids
    /// </summary>
    /// <param name="item"></param>
    /// <param name="successfullyAddedItem"></param>
    /// <param name="modifiedStackPivots"></param>
    /// <returns></returns>
    public override Item tryToAdd(Item item, out Item successfullyAddedItem, out Coordinate[] modifiedStackPivots) {
      int barSlotIndex = 0;
      int firstEmptySlot = EmptyGridSlot;
      Item itemsLeftToAdd = item;
      List<Coordinate> modifiedPivots = new List<Coordinate>();
      successfullyAddedItem = null;
      foreach (int[] barSlotStack in stackSlotGrid) {
        /// stop if we've run out of active bar
        if (barSlotIndex >= dimensions.x) {
          break;
        }
        // if this slot is empty, mark it for if we dont' find an existing stack
        if (barSlotStack[0] == EmptyGridSlot) {
          if (firstEmptySlot == EmptyGridSlot) {
            firstEmptySlot = barSlotIndex;
          }
        // if the slot is full but matches the item
        } else if (!stacks[barSlotStack[0]].isFull && stacks[barSlotStack[0]].canStackWith(itemsLeftToAdd)) {
          Coordinate slotLocation = (barSlotIndex, 0);
          modifiedPivots.Add(slotLocation);
          itemsLeftToAdd = addToStack(itemsLeftToAdd, slotLocation, out Item itemsAdded);
          if (itemsAdded != null) {
            successfullyAddedItem = successfullyAddedItem == null
              ? itemsAdded
              : successfullyAddedItem.addToStack(itemsAdded, out _);
          }
        }

        // stop when we run out
        if (itemsLeftToAdd == null) {
          break;
        }

        barSlotIndex++;
      }

      // if we have leftovers and found an empty slot stick it there
      if (itemsLeftToAdd != null && firstEmptySlot != EmptyGridSlot) {
        Coordinate slotLocation = (firstEmptySlot, 0);
        modifiedPivots.Add(slotLocation);
        addStackToSlot(itemsLeftToAdd, slotLocation);
        successfullyAddedItem = item;

        itemsLeftToAdd = null;
      }

      modifiedStackPivots = modifiedPivots.ToArray();
      return itemsLeftToAdd;
    }

    /// <summary>
    /// Remove the item from the slot
    /// </summary>
    /// <param name="slotIndex"></param>
    /// <returns></returns>
    public Item removeAt(int slotIndex) {
      return removeAt((slotIndex, 0));
    }

    public override Item tryToAdd(Item item, Coordinate barAndDeepSlot, out Item successfullyAddedItem) {
      (int barSlot, int deepSlot) = barAndDeepSlot;

      /// if it's within the allowed slots
      if (barSlot < dimensions.x) {
        // if it's the main bar slot or we've expanded this far already into the deep spots, just swap out the item.
        if (deepSlot == 0) {
          return tryToSwapOut(barAndDeepSlot, item, out successfullyAddedItem);
        }
      }

      /// failed to add the item, return it as leftover
      successfullyAddedItem = null;
      return item;
    }

    #endregion

    #region Serialization

    /// <summary>
    /// deserialization
    /// </summary>
    /// <param name="slotCount"></param>
    public SlotBasedInventory(NetworkReader reader) : base(reader) {}

    #endregion
  }
}