﻿using SpiritWorld.Game;
using SpiritWorld.World.Entities.Creatures;
using SpiritWorld.Game.Controllers;
using SpiritWorld.Game.Data;
using System.Collections.Generic;

namespace SpiritWorld {

	/// <summary>
	/// The client universe object
	/// </summary>
	public static class Saturn {

		/// <summary>
		/// Used to control scene transitions and meta changes
		/// </summary>
		public static Demiurge Demiurge;

		/// <summary>
		/// The controller of the local player.
		/// </summary>
		public static PlayerController LocalPlayerController;

		/// <summary>
		/// The controller of the current scape
		/// </summary>
		public static ScapeController ScapeController;

		/// <summary>
		/// The active player controllers
		/// </summary>
		public static Dictionary<Player, PlayerController> PlayerControllers {
			get; internal set;
		} = new Dictionary<Player, PlayerController>();

    /// <summary>
    /// The client's current loaded profile data
    /// </summary>
    public static class LoadedProfiles {

			/// <summary>
			/// The current user.
			/// </summary>
			public static UserProfile CurrentUser;

			/// <summary>
			/// The current user.
			/// </summary>
			public static CharacterProfile SelectedCharacter;

			/// <summary>
			/// The selected scape if there's one loaded
			/// </summary>
			public static ScapeProfile LoadedScape;

			/// <summary>
			/// Try to log in to the given user.
			/// </summary>
			/// <param name="username"></param>
			/// <param name="password"></param>
			/// <returns></returns>
			public static bool TryToLogIn(string username, string password) {
				// TODO if login fails set the profile to an empty dummy one.
				CurrentUser = UserProfile.GetDummyProfile(username);
				return true;
			}
		}
	}
}