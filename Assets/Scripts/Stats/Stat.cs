﻿using System;
using UnityEngine;

namespace SpiritWorld.Stats {

  /// <summary>
  /// A stat value struct
  /// </summary>
  [System.Serializable]
  public partial struct Stat {

    /// <summary>
    /// Groupings for different stats according to how variation points are dolled out
    /// </summary>
    public enum VariationGroups {
      Depleteable,
      Combat,
      Perception,
      Spendable,
      Weapon
    }

    /// <summary>
    /// The type of stat
    /// </summary>
    public Type type
      => _type ?? (_type = Types.Get(Id));
    Type _type;
    [SerializeField]
    byte Id;

    /// <summary>
    /// The current stat value
    /// </summary>
    public int current
      => Current;
    [SerializeField]
    int Current;

    /// <summary>
    /// The max value of this stat
    /// </summary>
    public int max
      => Max;
    [SerializeField]
    int Max;

    /// <summary>
    /// the min value of this stat
    /// </summary>
    public int min
      => Min;
    [SerializeField]
    int Min;

    /// <summary>
    /// Make a stat
    /// </summary>
    /// <param name="type"></param>
    /// <param name="max"></param>
    /// <param name="current"></param>
    /// <param name="min"></param>
    public Stat(Type type, int max, int current = 0, int min = 0) {
      Id = type.Id;
      _type = type;
      Max = max;
      Min = min;
      Current = Math.Min(current, max);
    }

    /// <summary>
    /// Update the current stat.
    /// </summary>
    /// <param name="oldStat"></param>
    /// <param name="newMax"></param>
    public Stat(Stat oldStat, int newMax) {
      Id = oldStat.type.Id;
      _type = oldStat.type;
      Max = newMax;
      Min = oldStat.min;
      Current = newMax;
    }
  }
}
