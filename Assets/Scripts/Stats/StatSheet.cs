﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace SpiritWorld.Stats {

  /// <summary>
  /// A basic sheet of stats
  /// </summary>
  [JsonObject]
  public abstract class StatSheet : Dictionary<int, Stat> {

    /// <summary>
    /// Block set. Make get return empty stats as default
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public Stat this[Stat.Type type] {
      get => TryGetValue(type.Id, out Stat stat) 
        ? stat 
        : default;
      protected set => base[type.Id] = value;
    }
  }
}
