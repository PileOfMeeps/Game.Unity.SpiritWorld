﻿/// <summary>
/// Various constants shared between the server and client
/// </summary>
public static class Universe {

	// TODO: buildable shrines and structures to attract more spirits or even trap them.
	// gives you more things to do with scape resources~

	/// <summary>
	/// The radius of a hexagon
	/// </summary>
  public const float HexRadius = 1;

	/// <summary>
	/// The height of a hexagon step
	/// </summary>
  public const float StepHeight = 1.0f / 6.0f;

	/// <summary>
	/// Current game version
	/// </summary>
	public const string GameVersion
		= "dev.0.0";
}
