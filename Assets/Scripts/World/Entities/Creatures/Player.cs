﻿using Newtonsoft.Json.Linq;
using SpiritWorld.Game.Data;
using SpiritWorld.Inventories;
using SpiritWorld.Inventories.Items;
using SpiritWorld.Stats;
using System;
using System.Collections.Generic;

namespace SpiritWorld.World.Entities.Creatures {

  /// <summary>
  /// A player entity
  /// </summary>
  public class Player : Creature, IEquatable<Player> {

    /// <summary>
    /// Inventory types the player has to store items
    /// </summary>
    public enum InventoryTypes {
      None,
      HotBar,
      Pockets,
      GridPack,
      Shortcuts
    }

    /// <summary>
    /// Slots on the player where different IEquipable items can be worn
    /// </summary>
    public enum EquiptmentSlots {
      Head,
      Chest,
      Legs,
      Neck,
      Ring, // there are 2 ring slots
    }

    /// <summary>
    /// The ways a weapon can be held
    /// </summary>
    public enum WeaponSlots {
      Hand, // left or right
      BothHands,
      Claws // players can't use this one
    }

    /// <summary>
    /// Hands a player can hold weapons in, wear rings on etc
    /// </summary>
    public enum Hands {
      Left,
      Right
    }

    #region Constants

    /// <summary>
    /// A player's empty hand as a tool
    /// </summary>
    public static ITool EmptyHand 
      = new Hand();

    #endregion

    /// <summary>
    /// The character profile associated with this player
    /// </summary>
    public CharacterProfile profile {
      get;
    }

    /// <summary>
    /// How many stat points the player has to spend on combat stats
    /// </summary>
    public readonly Stat availableStatPoints
      = new Stat(Stat.Types.AvailableStatPoints, 21, 6);

    /// <summary>
    /// Points spendable on abilities
    /// </summary>
    public readonly Stat abilityPoints
      = new Stat(Stat.Types.AbilityPoints, 10, 10);

    /// <summary>
    /// The players inventories
    /// </summary>
    public IInventory[] allInventories {
      get;
    }

    /// <summary>
    /// The players back pack inventory
    /// </summary>
    public ShapedPack packInventory {
      get;
      protected set;
    } = new ShapedPack((4, 7), new (Item, Coordinate)[] {
      (new Item(Item.Types.Wood, 2), (2,4)),
      (new Item(Item.Types.Wood, 2), (2,3)),
      (new Item(Item.Types.Wood, 2), (2,2))
    });

    /// <summary>
    /// The pocket inventory
    /// </summary>
    public readonly ItemBar pocketInventory 
      = new ItemBar(10, 1, 5);

    /// <summary>
    /// An inventory to hold the hot bar items.
    /// </summary>
    public readonly ItemBar hotBarInventory 
      = new ItemBar(10, 1, 5, new Item[] {
        Tool.Shortcuts[Tool.Type.Multi],
        new Item(Item.Types.Spapple, 2),
        new Item(Item.Types.Iron, 2),
        new Item(Item.Types.PineCone, 2),
      });

    /// <summary>
    /// Initialize a new player
    /// </summary>
    /// <param name="name"></param>
    public Player(string name) : base(Types.Player, name) {
      allInventories = new IInventory[] {
        hotBarInventory,
        pocketInventory,
        packInventory
      };
    }

    /// <summary>
    /// Set up a default player object
    /// </summary>
    public Player() : base(Types.Player, "") {}

    /// <summary>
    /// Get the serialized fields for this creatre.
    /// </summary>
    /// <returns></returns>
    public override Dictionary<string, JObject> getSerializedFields() {
      Dictionary<string, JObject> serializedFields = base.getSerializedFields();
      serializedFields["CombatStats"] = JObject.FromObject(combatStats);
      serializedFields["CombatStats"] = JObject.FromObject(combatStats);
      serializedFields["CombatStats"] = JObject.FromObject(combatStats);
      serializedFields["CombatStats"] = JObject.FromObject(combatStats);

      return serializedFields;
    }

    /// <summary>
    /// Get if the players are the same player
    /// </summary>
    /// <param name="other"></param>
    /// <returns></returns>
    public bool Equals(Player other) {
      return other.profile.id == profile.id;
    }

    /// <summary>
    /// Get if the players are the same player
    /// </summary>
    /// <param name="other"></param>
    /// <returns></returns>
    public override int GetHashCode() {
      return profile.id.GetHashCode();
    }
  }

  /// <summary>
  /// Type for bare hand tool
  /// </summary>
  public struct Hand : ITool {
    public Tool.Type ToolType 
      => Tool.Type.None;

    public int UpgradeLevel 
      => 0;

    public string UpgradeLevelName
      => "Bare Hands";

    public short NumberOfUses 
      => UseableItem.Type.UnlimitedUses;
  }
}

/// <summary>
/// Player type stuff
/// </summary>
namespace SpiritWorld.World.Entities {
  public partial class Creature {

    public static partial class Types {
      public static PlayerType Player = new PlayerType();
    }

    public class PlayerType : Type {
      internal PlayerType() : base(100, "Explorer") { }
    }
  }
}