﻿using System;

namespace SpiritWorld.World.Terrain.TileGrid {
  public static class HexGridShaper {

    /// <summary>
    /// Do an action on every axial location in a rectangular area
    /// </summary>
    /// <param name="action"></param>
    public static void Rectangle((int width, int depth) size, Action<Coordinate> action) {
      for (int x = 0; x < size.width; x++) {
        int xOffset = x >> 1;
        for (int y = -xOffset; y < size.depth - xOffset; y++) {
          action((x, y));
        }
      }
    }

    /// <summary>
    /// Do an action on every axial location in a diamond shaped area
    /// </summary>
    /// <param name="action">A function to run on each axial coordinate</param>
    public static void Diamond((int width, int depth) size, Action<Coordinate> action) {
      for (int y = 0; y < size.depth; y++) {
        int yOffset = y >> 1;
        for (int x = -yOffset; x < size.width - yOffset; x++) {
          action((x, y));
        }
      }
    }

    /// <summary>
    /// Do an action on every axial location in a hexagon shaped area
    /// </summary>
    /// <param name="radius"></param>
    /// <param name="action"></param>
    public static void Hexagon(int radius, Action<Coordinate> action) {
      for (int y = -radius; y <= radius; y++) {
        int r1 = Math.Max(-radius, -y - radius);
        int r2 = Math.Min(radius, -y + radius);
        for (int x = r1; x <= r2; x++) {
          action((x, y));
        }
      }
    }
  }
}
