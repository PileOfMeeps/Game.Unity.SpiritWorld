﻿using SpiritWorld.World.Terrain.TileGrid.Generation;
using UnityEngine;

namespace SpiritWorld.World.Terrain.TileGrid {
  public class HexagonalBoard : TileBoard {

    /// <summary>
    /// A chunks diameter in hex tiles
    /// </summary>
    public const int ChunkRadiusInTiles = 36;

    /// <summary>
    /// The size of a chunk in square game units
    /// </summary>
    public static Vector3 ChunkWorldOffset = new Vector3(ChunkRadiusInTiles * 2, 0, 62.35f);

    /// <summary>
    /// The local center location of the chunk
    /// </summary>
    public static Vector3 ChunkWorldCenter = ChunkWorldOffset / 2;

    /// <summary>
    /// Make a hexagonal board
    /// </summary>
    /// <param name="biome"></param>
    public HexagonalBoard(Biome biome) : base(Shapes.Hexagon, biome) {}

    /// <summary>
    /// Add a new hexagonal chunk of grid to the tile board
    /// </summary>
    public override void createNewGrid(Coordinate chunkAxialKey) {
      HexGridChunk newGrid = new HexGridChunk();
      HexGridShaper.Hexagon(16, axialKey => {
        newGrid.set(biome.generateAt(axialKey, (chunkAxialKey)));
      });

      Add(chunkAxialKey, newGrid);
    }

    public override Tile get(Vector3 worldPosition, out Coordinate containingChunkKey) {
      // get which chunk we hit
      containingChunkKey = getChunkKeyFromWorldPosition(worldPosition);
      HexGridChunk containingChunk = this[containingChunkKey];
      Vector3 chunkWorldOffset = ChunkWorldOffset * containingChunkKey;
      Vector3 localChunkPosition = worldPosition - chunkWorldOffset;

      // get the axial of the selected hex.
      Coordinate localHexAxialKey = Hexagon.WorldLocationToAxialKey(localChunkPosition);
      Tile foundTile = containingChunk.get(localHexAxialKey);

      return foundTile;
    }

    public override Coordinate getChunkKeyFor(Tile tile) {
      Coordinate containingChunkKey;
      containingChunkKey = getChunkKeyFromWorldPosition(tile.worldLocation);
      Tile foundTile = this[containingChunkKey]?.get(tile.axialKey) ?? default;

      return containingChunkKey;
    }

    /// <summary>
    /// Get the board location of a chunk on a hexagonal board that contains the given position
    /// </summary>
    /// <param name="worldPosition"></param>
    /// <returns></returns>
    Coordinate getChunkKeyFromWorldPosition(Vector3 worldPosition) {
      Hexagon.WorldLocationToAxialKey(worldPosition / ChunkRadiusInTiles);
      return (
        (int)Mathf.Floor(worldPosition.x / ChunkWorldOffset.x),
        (int)Mathf.Floor(worldPosition.z / ChunkWorldOffset.z)
      );
    }
  }
}
