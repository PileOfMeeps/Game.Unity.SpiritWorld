﻿using SpiritWorld.Game.Data;
using SpiritWorld.World.Terrain.TileGrid;
using System.Collections.Generic;

namespace SpiritWorld.World {

  /// <summary>
  /// A Level/Land/World in the game, made up of entities, tile boards, and structures.
  /// </summary>
  public class Scape {

    /// <summary>
    /// Types of scapes
    /// </summary>
    public enum Type {
      Home,
      Endless
    }

    #region Constants

    /// <summary>
    /// The index of the parent board of all other boards in this scape
    /// </summary>
    public const int MainBoardIndex
      = 0;

    #endregion

    /// <summary>
    /// The serialized scape data, such as name, seeds, etc
    /// </summary>
    public ScapeProfile data {
      get;
    }

    /// <summary>
    /// The main/base board of the level, from which other boards stem
    /// </summary>
    public TileBoard mainBoard
      => boards[MainBoardIndex];

    /// <summary>
    /// The tile boards that make up this worldscape
    /// </summary>
    List<TileBoard> boards 
      = new List<TileBoard>();


    /// <summary>
    /// Make a scape based on the profile
    /// </summary>
    /// <param name="dataProfile"></param>
    public Scape(ScapeProfile dataProfile) {
      data = dataProfile;
    }

    /// <summary>
    /// Get a board from this scape by id
    /// </summary>
    /// <param name="boardId"></param>
    /// <returns></returns>
    public TileBoard getBoard(int boardId) {
      return boards[boardId];
    }

    /// <summary>
    /// Add a board to this worldscape
    /// </summary>
    /// <param name="board"></param>
    /// <param name="index"></param>
    public void addBoard(TileBoard board, int? index = null) {
      if (index == null) {
        boards.Add(board);
      } else {
        boards.Insert((int)index, board);
      }
    }
  }
}
